<?php

namespace CMS\HotelsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HousingTypesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'cms_localization_text_type')
            ->add('is_have_rooms', 'checkbox');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\HotelsBundle\Entity\HousingTypes'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cms_hotelsbundle_housingtypes';
    }
}
