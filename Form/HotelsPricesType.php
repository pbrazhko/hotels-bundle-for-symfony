<?php

namespace CMS\HotelsBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HotelsPricesType extends AbstractType
{
    /**
     * @var integer
     */
    private $hotel;

    /**
     * @var integer
     */
    private $room;

    public function __construct($hotel = null, $room = null){
        $this->hotel = $hotel;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $hotel = $this->hotel;
        $room = $this->room;

        if (null === $hotel) {
            $builder
                ->add('room', 'cms_localization_entity_type', array(
                    'class' => 'HotelsBundle:Hotels',
                    'property' => 'title',
                    'empty_data' => null,
                    'placeholder' => '',
                ));
        }

        if(null === $room) {
            $builder
                ->add('room', 'cms_localization_entity_type', array(
                    'class' => 'HotelsBundle:HotelsRooms',
                    'query_builder' => function (EntityRepository $er) use ($hotel) {
                        return $er->createQueryBuilder('r')
                            ->where('r.hotel = :hotel')
                            ->setParameter('hotel', $hotel);
                    },
                    'property' => 'title',
                    'empty_data' => null,
                    'placeholder' => '',
                ));
        }

        $builder
            ->add('guests')
            ->add('date', 'datetime', array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd.MM.yyyy',
                'attr' => array(
                    'class' => 'datepicker'
                )
            ))
            ->add('price')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\HotelsBundle\Entity\HotelsPrices'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cms_hotelsbundle_hotelsprices';
    }
}
