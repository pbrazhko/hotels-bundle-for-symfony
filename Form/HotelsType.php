<?php

namespace CMS\HotelsBundle\Form;

use CMS\GalleryBundle\Form\Types\ImagesType;
use CMS\GeoBundle\Form\Types\GeoObjectType;
use CMS\LocalizationBundle\Form\Types\LocaleEntityType;
use CMS\LocalizationBundle\Form\Types\LocaleTextareaType;
use CMS\LocalizationBundle\Form\Types\LocaleTextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HotelsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', LocaleTextType::class)
            ->add('city', LocaleEntityType::class, array(
                'class' => 'LocalizationBundle:Cities',
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => '',
            ))
            ->add('housing_type', LocaleEntityType::class, array(
                'class' => 'HotelsBundle:HousingTypes',
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => '',
            ))
            ->add('descriptions', LocaleTextareaType::class, array(
                'required' => false
            ))
            ->add('photos', ImagesType::class, [
                'required' => false
            ])
            ->add('location', GeoObjectType::class)
            ->add('is_published')
            ->add('is_deleted');

        if (true === $options['show_rooms']) {
            $builder
                ->add('rooms', CollectionType::class, [
                    'entry_type' => HotelsRoomsType::class
                ]);
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'hotels',
            'data_class' => 'CMS\HotelsBundle\Entity\Hotels',
            'show_rooms' => false
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_hotelsbundle_simple_hotels';
    }
}
