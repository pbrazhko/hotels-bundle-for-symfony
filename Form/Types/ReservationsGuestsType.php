<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 08.06.15
 * Time: 17:36
 */

namespace CMS\HotelsBundle\Form\Types;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReservationsGuestsType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'entry_type' => ReservationsGuestType::class,
            'allow_add' => true,
            'allow_delete' => true,
            'delete_empty' => true
        ));
    }

    public function getParent()
    {
        return CollectionType::class;
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getBlockPrefix()
    {
        return 'cms_hotels_reservations_guests_type';
    }

}