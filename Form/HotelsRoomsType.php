<?php

namespace CMS\HotelsBundle\Form;

use CMS\GalleryBundle\Form\Types\ImagesType;
use CMS\LocalizationBundle\Form\Types\LocaleEntityType;
use CMS\LocalizationBundle\Form\Types\LocaleTextareaType;
use CMS\LocalizationBundle\Form\Types\LocaleTextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HotelsRoomsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', LocaleTextType::class)
            ->add('count_rooms')
            ->add('count_bedrooms')
            ->add('count_bathrooms')
            ->add('price_day')
            ->add('price_additional_guest')
            ->add('count_guests')
            ->add('count_max_guests')
            ->add('descriptions', LocaleTextareaType::class, array(
                'required' => false
            ))
            ->add('duration_reservation')
            ->add('reservation_min_days')
            ->add('services', LocaleEntityType::class, array(
                'class' => 'HotelsBundle:Services',
                'choice_label' => 'title',
                'multiple' => true,
                'expanded' => true
            ))
            ->add('photos', ImagesType::class)
            ->add('count')
            ->add('is_published')
            ->add('is_deleted');
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\HotelsBundle\Entity\HotelsRooms',
            'validation_groups' => ['default'],
            'translation_domain' => 'hotels'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_hotelsbundle_hotelsrooms';
    }
}
