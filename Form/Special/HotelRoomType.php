<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 26.11.15
 * Time: 17:15
 */

namespace CMS\HotelsBundle\Form\Special;


use CMS\HotelsBundle\Form\HotelsRoomsType;
use CMS\HotelsBundle\Form\HotelsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class HotelRoomType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('hotel', new HotelsType())
            ->add('room', new HotelsRoomsType());
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'cms_hotelsbundle_hotel_room_type';
    }
}