<?php

namespace CMS\HotelsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReservationsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('hotel', 'hidden')
            ->add('room', 'hidden')
            ->add('price', 'hidden')
            ->add('date_start', 'date', array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd.MM.yyyy',
                'model_timezone' => 'UTC',
                'attr' => array(
                    'class' => 'hide',
                    'autocomplete' => 'off'
                )
            ))
            ->add('date_end', 'date', array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd.MM.yyyy',
                'model_timezone' => 'UTC',
                'attr' => array(
                    'class' => 'hide',
                    'autocomplete' => 'off'
                )
            ))
            ->add('customer_name')
            ->add('customer_last_name')
            ->add('customer_phone')
            ->add('customer_email')
            ->add('adults', 'cms_hotels_reservations_guests_type')
            ->add('comments');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'hotels',
            'error_bubbling' => false,
            'data_class' => 'CMS\HotelsBundle\Entity\HotelsReservations'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cms_hotelsbundle_reservations';
    }
}
