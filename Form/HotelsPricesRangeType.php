<?php

namespace CMS\HotelsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class HotelsPricesRangeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date_start', 'datetime', array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd.MM.yyyy',
                'attr' => array(
                    'class' => 'datepicker'
                )
            ))
            ->add('date_end', 'datetime', array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd.MM.yyyy',
                'attr' => array(
                    'class' => 'datepicker'
                )
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cms_hotelsbundle_hotelspricesrange';
    }
}
