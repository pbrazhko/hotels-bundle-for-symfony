<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 03.12.15
 * Time: 10:28
 */

namespace CMS\HotelsBundle\Provider;


use CMS\GalleryBundle\Normalizer\ImagesNormalizer;
use CMS\HotelsBundle\HotelCriteria;
use CMS\HotelsBundle\Normalizers\HotelsNormalizer;
use CMS\HotelsBundle\Normalizers\HotelsRoomsNormalizer;
use CMS\HotelsBundle\Normalizers\HotelsRoomsServiceNormalizer;
use CMS\HotelsBundle\Normalizers\HousingTypeNormalizer;
use CMS\HotelsBundle\Services\HotelsService;
use CMS\LocalizationBundle\Normalizer\CitiesNormalizer;
use Elastica\Document;
use Elastica\Exception\ElasticsearchException;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\Match;
use Elastica\Type;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use FOS\ElasticaBundle\HybridResult;
use FOS\ElasticaBundle\Provider\ProviderInterface;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Serializer\Serializer;

class ElasticaHotelsProvider implements ProviderInterface
{

    /**
     * @var Type
     */
    private $hotelsType;

    /**
     * @var TransformedFinder
     */
    private $hotelFinder;

    /**
     * @var HotelsService
     */
    private $hotelsService;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var Serializer
     */
    private $serializer;


    /**
     * ElasticaHotelsProvider constructor.
     * @param Type $hotelsType
     * @param HotelsService $hotelsService
     * @param TransformedFinder $hotelFinder
     * @param Logger $logger
     */
    public function __construct(
        Type $hotelsType,
        HotelsService $hotelsService,
        TransformedFinder $hotelFinder,
        Logger $logger
    )
    {
        $this->hotelsType = $hotelsType;
        $this->hotelsService = $hotelsService;
        $this->hotelFinder = $hotelFinder;
        $this->logger = $logger;

        $normalizers = [
            new HotelsNormalizer(),
            new HotelsRoomsNormalizer(),
            new HousingTypeNormalizer(),
            new CitiesNormalizer(),
            new HotelsRoomsServiceNormalizer(),
            new ImagesNormalizer()
        ];

        $this->serializer = new Serializer($normalizers, []);
    }

    public function findById($hotelId){
        $query = new BoolQuery();

        $fieldQuery = new Match();
        $fieldQuery->setField('id', $hotelId);

        $query->addShould($fieldQuery);

        return $this->hotelFinder->findHybrid($query);
    }

    public function addHotelToIndex($hotelId){

        $results = $this->findById($hotelId);

        if(!empty($results)){
            return $this->updateHotelIndex($hotelId);
        }

        if(null !== $hotel = $this->hotelsService->findOneBy(['id' => $hotelId])){
            try {
                $this->hotelsType->addDocument(
                    (new Document())->setData(
                        $this->serializer->normalize($hotel)
                    )
                );
            }
            catch (ElasticsearchException $e){
                $this->logger->addCritical($e->getMessage());
                return;
            }
        }
    }

    public function updateHotelIndex($hotelId){

        $results = $this->findById($hotelId);

        if(empty($results)){
            return $this->addHotelToIndex($hotelId);
        }

        /** @var HybridResult $result */
        foreach ($results as $result) {
            try {
                $this->hotelsType->updateDocument(
                    (new Document($result->getResult()->getId()))->setData($this->serializer->normalize($result->getTransformed()))
                );
            }
            catch (ElasticsearchException $e){
                $this->logger->addCritical($e->getMessage());
                return;
            }
        }
    }

    /**
     * Persists all domain objects to ElasticSearch for this provider.
     *
     * The closure can expect 2 or 3 arguments:
     *   * The step size
     *   * The total number of objects
     *   * A message to output in error conditions (not normally provided)
     *
     * @param \Closure $loggerClosure
     * @param array $options
     *
     * @return void
     */
    public function populate(\Closure $loggerClosure = null, array $options = array())
    {
        $hotelCriteria = new HotelCriteria();
        $hotelCriteria
            ->setIsSelectPhotos(true)
            ->setIsSelectRooms(true)
            ->setIsSelectServices(true)
            ->setIsDeleted(false)
            ->setIsPublished(true);

        $hotels = $this->hotelsService->getRepository()->getHotels($hotelCriteria);

        if($hotels){
            $documents = [];

            foreach($hotels as $hotel){
                $document = new Document();
                $document->setData(
                    $this->serializer->normalize($hotel, 'json')
                );

                $documents[] = $document;
            }

            $this->hotelsType->addDocuments($documents);
        }
    }
}