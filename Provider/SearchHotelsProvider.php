<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 22.06.15
 * Time: 16:43
 */

namespace CMS\HotelsBundle\Provider;

use CMS\HotelsBundle\Entity\HotelsReservations;
use CMS\HotelsBundle\Entity\Services;
use CMS\HotelsBundle\Services\HotelsPricesService;
use CMS\LocalizationBundle\Form\Types\LocaleEntityType;
use CMS\SearchBundle\Interfaces\SearchProviderInterface;
use CMS\SearchBundle\Services\SearchService;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Elastica\Query as EQ;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;

class SearchHotelsProvider implements SearchProviderInterface
{
    const SEARCH_TYPE = 'hotels';

    /**
     * @var TransformedFinder
     */
    private $hotelsFinder;

    /**
     * @var HotelsPricesService
     */
    private $hotelsPricesService;

    public function __construct(TransformedFinder $hotelsFinder, HotelsPricesService $hotelsPricesService)
    {
        $this->hotelsFinder = $hotelsFinder;
        $this->hotelsPricesService = $hotelsPricesService;
    }

    public function buildFilterForm(FormBuilderInterface $builder, array $defaultFieldsData = array())
    {
        $builder
            ->add('title', TextType::class, array(
                'required' => false,
                'attr' => array(
                    'placeholder' => 'Title'
                ),
                'translation_domain' => 'search'
            ))
            ->add('services', LocaleEntityType::class, array(
                'class' => 'HotelsBundle:Services',
                'choice_label' => 'title',
                'multiple' => true,
                'expanded' => true,
                'required' => false,
                'translation_domain' => 'search'
            ))
            ->add('housing_type', LocaleEntityType::class, array(
                'class' => 'HotelsBundle:HousingTypes',
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => '',
                'required' => false,
                'translation_domain' => 'search'
            ))
            ->add('count_rooms', TextType::class, array(
                'required' => false,
                'translation_domain' => 'search',
                'attr' => array(
                    'placeholder' => 'Count rooms'
                ),
            ))
            ->add('city', LocaleEntityType::class, array(
                'class' => 'LocalizationBundle:Cities',
                'choice_label' => 'title',
                'empty_data' => null,
                'placeholder' => '',
                'required' => false
            ))
            ->add('date_start', DateTimeType::class, array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd.MM.yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                    'placeholder' => 'Date start'
                ),
                'required' => false,
            ))
            ->add('date_end', DateTimeType::class, array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd.MM.yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                    'placeholder' => 'Date end'
                ),
                'required' => false,
            ))
            ->add('price_start', TextType::class, [
                'attr' => [
                    'placeholder' => 'Price start'
                ],
                'required' => false,
                'data' => 10
            ])
            ->add('price_end', TextType::class, [
                'attr' => [
                    'placeholder' => 'Price end'
                ],
                'required' => false,
                'data' => 1000
            ])
            ->add('count_guests', TextType::class, [
                'attr' => [
                    'placeholder' => 'Count guests'
                ],
                'required' => false,
            ]);
    }

    /**
     * @param Form $form
     * @param SearchService $searchService
     * @return mixed
     */
    public function search(Form $form, SearchService $searchService)
    {
        $dateStart = $this->getFormData($form, 'date_start');
        $dateEnd = $this->getFormData($form, 'date_end');

        $allowedHotels = [];

        $reservedHotels = $this->getAllowedRoom($dateStart, $dateEnd);

        if (count($reservedHotels)) {
            foreach ($reservedHotels as $reserved) {
                $allowedHotels[] = $reserved['hotel'];
            }
        }

        $boolQuery = new EQ\BoolQuery();

        $queryTerm = new EQ\Terms();
        $queryTerm->setTerms('id', $allowedHotels);

        $boolQuery->addMust($queryTerm);

        if (null !== ($title = $this->getFormData($form, 'title'))) {

            $queryTerm = new EQ\Term();
            $queryTerm->setTerm('title.' . $searchService->getLocale(), mb_strtolower($title));

            $boolQuery->addMust($queryTerm);
        }

        if ($form->has('services') && count($form->get('services')->getData()) > 0) {

            $servicesId = [];
            /** @var Services $service */
            foreach ($form->get('services')->getData() as $service) {
                $servicesId[] = $service->getId();
            }

            $queryTerms = new EQ\Terms();
            $queryTerms->setTerms('rooms.services.id', $servicesId);

            $boolQuery->addMust($queryTerms);
        }

        if (null !== ($housingType = $this->getFormData($form, 'housing_type'))) {
            $queryTerm = new EQ\Term();
            $queryTerm->setTerm('housing_type.id', $housingType->getId());

            $boolQuery->addMust($queryTerm);
        }

        if (null !== ($countRoom = $this->getFormData($form, 'count_rooms'))) {
            $queryRange = new EQ\Range();
            $queryRange->addField('rooms.count_rooms', ['gte' => $countRoom]);

            $boolQuery->addMust($queryRange);
        }

        if (null !== ($priceStart = $this->getFormData($form, 'price_start'))) {
            $queryRange = new EQ\Range();
            $queryRange->addField('rooms.price_day', ['gte' => $priceStart]);

            $boolQuery->addMust($queryRange);
        }

        if (null !== ($priceEnd = $this->getFormData($form, 'price_end'))) {
            $queryRange = new EQ\Range();
            $queryRange->addField('rooms.price_day', ['lte' => $priceEnd]);

            $boolQuery->addMust($queryRange);
        }

        if (null !== ($city = $this->getFormData($form, 'city'))) {
            $queryTerm = new EQ\Term();
            $queryTerm->setTerm('city.id', $city->getId());

            $boolQuery->addMust($queryTerm);
        }

        if (null !== ($countGuests = $this->getFormData($form, 'count_guests'))) {
            $queryRange = new EQ\Range();
            $queryRange->addField('rooms.count_guests', ['lte' => $countGuests]);

            $boolQuery->addMust($queryRange);

            $queryRange = new EQ\Range();
            $queryRange->addField('rooms.count_max_guests', ['gte' => $countGuests]);

            $boolQuery->addMust($queryRange);
        }


        $sort = ['rooms.price_day' => ['order' => 'desc']];

        if($searchService->getOrderBy() == 'rating'){
            $sort = [$searchService->getOrderBy() => ['order' => 'desc']];
        }

        $query = new \Elastica\Query($boolQuery);
        $query->addSort($sort);

        $result = $this->hotelsFinder->find($query, $searchService->getLimit());

        return $result;
    }


    public function searchOld(Form $form, SearchService $searchService)
    {
        $hotelsRepository = $this->container->get('cms.hotels.service')->getRepository();

        $dateStart = $this->getFormData($form, 'date_start');
        $dateEnd = $this->getFormData($form, 'date_end');

        $priceStart = $this->getFormData($form, 'price_start');
        $priceEnd = $this->getFormData($form, 'price_end');

        $notAllowedRooms = [];

        $reservedRooms = $this->getReservedRoom($dateStart, $dateEnd);

        if (count($reservedRooms)) {
            foreach ($reservedRooms as $reserved) {
                $notAllowedRooms[] = $reserved['room'];
            }
        }

        $builder = $hotelsRepository->createQueryBuilder('h');

        $builder
            ->select(array('h', 'r', 'hp'))
            ->leftJoin('h.photos', 'hp');

        if (count($notAllowedRooms)) {
            $builder
                ->innerJoin('h.rooms', 'r', Join::WITH, $builder->expr()->notIn('r.id', $notAllowedRooms));
        } else {
            $builder
                ->innerJoin('h.rooms', 'r');
        }

        if (null !== ($title = $this->getFormData($form, 'title'))) {
            $builder->andWhere($builder->expr()->like('h.title', ':title'));
            $builder->setParameter('title', '%' . $title . '%');
        }

        if ($form->has('services') && count($form->get('services')->getData()) > 0) {
            $builder->innerJoin(
                'r.services',
                'rs',
                Join::WITH,
                $builder->expr()->in('rs.id', ':services')
            );

            /** @var Services $service */
            foreach ($form->get('services')->getData() as $service) {
                $servicesId[] = $service->getId();
            }

            $builder->setParameter('services', $servicesId);
        }

        if (null !== ($housingType = $this->getFormData($form, 'housing_type'))) {
            $builder->andWhere('h.housing_type = :housing_type');
            $builder->setParameter('housing_type', $housingType);
        }

        if (null !== ($countRoom = $this->getFormData($form, 'count_rooms'))) {
            $builder->andWhere('r.count_rooms = :count_rooms');
            $builder->setParameter('count_rooms', $countRoom);
        }

        if (null !== ($city = $this->getFormData($form, 'city'))) {
            $builder->andWhere('h.city = :city');
            $builder->setParameter('city', $city);
        }

        if (null !== ($countGuests = $this->getFormData($form, 'count_guests'))) {
            //$builder->andWhere('r.count_guests  :count_guests');
            $builder->andWhere('r.count_max_guests >= :count_guests');
            $builder->setParameter('count_guests', $countGuests);
        }

        $builder
            ->andWhere('h.is_deleted = 0')
            ->andWhere('h.is_published = 1')
            ->andWhere('r.is_deleted = 0')
            ->andWhere('r.is_published = 1')
            ->addOrderBy('r.' . $searchService->getOrderBy())
            ->setFirstResult($searchService->getLimit() * ($searchService->getPage() - 1))
            ->setMaxResults($searchService->getLimit());

        $builder->addGroupBy('h.id');

        $query = $builder->getQuery();

        $hints = $query->getHint(Query::HINT_CUSTOM_TREE_WALKERS);

        $query->setHint(Query::HINT_CUSTOM_TREE_WALKERS, $hints);

        $result = $query->getResult();

        return $result;
    }

    private function getAllowedRoom($dateStart = null, $dateEnd = null)
    {
        if (null === $dateStart) {
            $dateStart = new \DateTime();
        }

        $repository = $this->hotelsPricesService->getRepository();

        $builder = $repository->createQueryBuilder('p');
        $builder
            ->select(['p.hotel', 'p.room', 'p.count', 'COUNT(DISTINCT r.id) as count_reservation_of_date'])
            ->andWhere('p.date >= :date_start')
            ->setParameter('date_start', $dateStart)
            ->leftJoin(
                'HotelsBundle:HotelsReservations',
                'r',
                Join::WITH,
                'p.date >= r.date_start AND p.date <= r.date_end AND r.hotel = p.hotel AND r.room = p.room AND r.is_deleted = 0 AND r.status = :status')
            ->groupBy('p.hotel')
            ->addGroupBy('p.room')
            ->addGroupBy('p.count')
            ->having('count_reservation_of_date < p.count');

        if ($dateEnd) {
            $builder
                ->andWhere('p.date <= :date_end')
                ->setParameter('date_end', $dateEnd);
        }

        $builder
            ->setParameter('status', HotelsReservations::RESERVATION_SUCCESS);


        $query = $builder->getQuery();

        return $query->getArrayResult();
    }

    private function getFormData(Form $form, $field, $default = null)
    {
        if ($form->has($field) && !$form->get($field)->isEmpty()) {
            return $form->get($field)->getData();
        }

        return $default;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return self::SEARCH_TYPE;
    }
}