<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 26.08.15
 * Time: 22:39
 */

namespace CMS\HotelsBundle\Provider;


use CMS\GeoBundle\GeoObjectsInfoInterface;
use CMS\GeoBundle\Response\GeoObjectIconResponse;
use CMS\GeoBundle\Response\GeoObjectsInfoResponse;
use CMS\GeoBundle\Response\GeoObjectsListResponse;
use CMS\HotelsBundle\HotelCriteria;
use CMS\HotelsBundle\Services\HotelsService;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpFoundation\Response;

class GeoHotelsProvider implements GeoObjectsInfoInterface
{
    private $hotelsService;

    private $templating;

    public function __construct(HotelsService $hotelsService, TwigEngine $templating)
    {
        $this->hotelsService = $hotelsService;
        $this->templating = $templating;
    }

    const TYPE_NAME = 'HOTEL';

    /**
     * Return information of object
     *
     * @param $geoObject
     * @return string
     */
    public function getGeoObjectsInfo($geoObject)
    {
        $criteria = new HotelCriteria();
        $criteria->setLocation($geoObject);
        $criteria->setIsPublished(true);
        $criteria->setIsOneResult(true);

        $response = null;

        if (null !== ($hotel = $this->hotelsService->getHotel($criteria))) {
            $response = $this->templating->render(':city_tour/hotels/specials:map_info.html.twig', [
                'hotel' => $hotel
            ]);
        }

        return new Response($response);
    }

    /**
     * Return list objects for this type
     *
     * @param string|null $query
     * @param int|null $limit
     * @param int|null $skip
     * @return GeoObjectsListResponse
     */
    public function getObjectsList($query = null, $limit = null, $skip = null)
    {
        return new GeoObjectsListResponse();
    }

    /**
     * Return icon for geo objects
     *
     * @return string|GeoObjectIconResponse
     */
    public function getIcon()
    {
        return new GeoObjectIconResponse();
    }

    /**
     * Return true is objects of type area
     *
     * @return boolean
     */
    public function isArea()
    {
        return false;
    }

    /**
     * Return name type for this provider
     *
     * @return string
     */
    public static function getTypeName()
    {
        return self::TYPE_NAME;
    }

}