<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 03.04.16
 * Time: 15:17
 */

namespace CMS\HotelsBundle\Validator;


use CMS\HotelsBundle\Services\HotelsPricesService;
use CMS\HotelsBundle\Services\ReservationsService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ReservationDateEndValidator extends ConstraintValidator
{
    /**
     * @var ReservationsService
     */
    private $reservationService;

    /**
     * @var HotelsPricesService
     */
    private $priceService;

    /**
     * ReservationDateStartValidator constructor.
     * @param ReservationsService $reservationService
     * @param HotelsPricesService $priceService
     */
    public function __construct(ReservationsService $reservationService, HotelsPricesService $priceService)
    {
        $this->reservationService = $reservationService;
        $this->priceService = $priceService;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        // TODO: Implement validate() method.
    }

}