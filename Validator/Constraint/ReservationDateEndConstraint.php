<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 03.04.16
 * Time: 15:13
 */

namespace CMS\HotelsBundle\Validator\Constraint;


use Symfony\Component\Validator\Constraint;

class ReservationDateEndConstraint extends Constraint
{
    public $message = 'Date cannot be selected';

    /**
     * Returns the name of the class that validates this constraint.
     *
     * By default, this is the fully qualified name of the constraint class
     * suffixed with "Validator". You can override this method to change that
     * behaviour.
     *
     * @return string
     */
    public function validatedBy()
    {
        return 'reservation_date_end.validator';
    }

}