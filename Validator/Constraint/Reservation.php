<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 22.09.15
 * Time: 21:19
 */

namespace CMS\HotelsBundle\Validator\Constraint;


use Symfony\Component\Validator\Constraint;

class Reservation extends Constraint
{
    public $message = 'Max count guests in this hotel(room), is %max_guests%';

    /**
     * Returns whether the constraint can be put onto classes, properties or
     * both.
     *
     * This method should return one or more of the constants
     * Constraint::CLASS_CONSTRAINT and Constraint::PROPERTY_CONSTRAINT.
     *
     * @return string|array One or more constant values
     *
     * @api
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    /**
     * Returns the name of the class that validates this constraint.
     *
     * By default, this is the fully qualified name of the constraint class
     * suffixed with "Validator". You can override this method to change that
     * behaviour.
     *
     * @return string
     *
     * @api
     */
    public function validatedBy()
    {
        return 'ReservationValidator';
    }
}