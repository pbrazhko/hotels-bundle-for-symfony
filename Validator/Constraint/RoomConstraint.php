<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 09.04.16
 * Time: 19:28
 */

namespace CMS\HotelsBundle\Validator\Constraint;


use Symfony\Component\Validator\Constraint;

class RoomConstraint extends Constraint
{
    public $message = 'Room %s not found';

    public $hotelPath = 'hotel';
    /**
     * Returns the name of the class that validates this constraint.
     *
     * By default, this is the fully qualified name of the constraint class
     * suffixed with "Validator". You can override this method to change that
     * behaviour.
     *
     * @return string
     */
    public function validatedBy()
    {
        return 'room.validator';
    }

    /**
     * Returns the name of the required options.
     *
     * Override this method if you want to define required options.
     *
     * @return array
     *
     * @see __construct()
     */
    public function getRequiredOptions()
    {
        return ['hotelPath'];
    }


}