<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 09.04.16
 * Time: 19:28
 */

namespace CMS\HotelsBundle\Validator\Constraint;


use Symfony\Component\Validator\Constraint;

class HotelConstraint extends Constraint
{
    public $message = 'Hotel %s not found';

    /**
     * Returns the name of the class that validates this constraint.
     *
     * By default, this is the fully qualified name of the constraint class
     * suffixed with "Validator". You can override this method to change that
     * behaviour.
     *
     * @return string
     */
    public function validatedBy()
    {
        return 'hotel.validator';
    }
}