<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 03.04.16
 * Time: 15:17
 */

namespace CMS\HotelsBundle\Validator;


use CMS\HotelsBundle\Entity\Hotels;
use CMS\HotelsBundle\Entity\HotelsRooms;
use CMS\HotelsBundle\Services\HotelsPricesRepository;
use CMS\HotelsBundle\Services\HotelsRoomsService;
use CMS\HotelsBundle\Services\HotelsService;
use CMS\HotelsBundle\Services\ReservationsRepository;
use Symfony\Component\Form\Form;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class RoomValidator extends ConstraintValidator
{
    /**
     * @var HotelsRoomsServicece
     */
    private $roomsService;

    /**
     * RoomValidator constructor.
     * @param HotelsRoomsService $roomsService
     */
    public function __construct(HotelsRoomsService $roomsService)
    {
        $this->roomsService = $roomsService;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var HotelsRooms $room */
        $room = $this->roomsService->getRoomById($value);

        /** @var Form $form */
        $form = $this->context->getRoot();

        $hotel = $form->get($constraint->hotelPath)->getData();

        if($hotel instanceof Hotels){
            $hotel = $hotel->getId();
        }

        if($room->getHotel()->getId() != $hotel){
            $this->context->buildViolation(
                sprintf('Hotel is not have room with ID %s', $room)
            )->atPath('room')->addViolation();
        }

        if(!$room){
            $this->context->buildViolation(
                sprintf($constraint->message, $value)
            )->atPath('room')->addViolation();
        }
    }

}