<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 03.09.15
 * Time: 16:25
 */

namespace CMS\HotelsBundle\Validator;


use CMS\HotelsBundle\Entity\Hotels;
use CMS\HotelsBundle\Entity\HotelsPrices;
use CMS\HotelsBundle\Entity\HotelsReservations;
use CMS\HotelsBundle\PriceCriteria;
use CMS\HotelsBundle\ReservationCriteria;
use CMS\HotelsBundle\Services\HotelsPricesService;
use CMS\HotelsBundle\Services\HotelsService;
use CMS\HotelsBundle\Services\ReservationsService;
use CMS\HotelsBundle\Validator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ReservationValidator extends ConstraintValidator
{
    private $pricesService;
    private $reservationService;
    private $hotelsService;

    public function __construct(HotelsPricesService $pricesService, ReservationsService $reservationsService, HotelsService $hotelsService)
    {
        $this->pricesService = $pricesService;
        $this->reservationService = $reservationsService;
        $this->hotelsService = $hotelsService;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     *
     * @api
     */
    public function validate($value, Constraint $constraint)
    {

        if ($value->getRoom()) {
            /** @var Hotels $hotel */
            if (null === ($hotel = $this->hotelsService->getHotelByIdAndRoom($value->getHotel(), $value->getRoom()))) {
                $this->context->buildViolation('Hotel not found')->atPath('hotel')->addViolation();
            }

            $rooms = $hotel->getRooms()->toArray();
            $room = reset($rooms);
        }
        else {
            /** @var Hotels $hotel */
            if (null === ($hotel = $this->hotelsService->getHotelById($value->getHotel()))) {
                $this->context->buildViolation('Hotel not found')->atPath('hotel')->addViolation();
            }
            $room = null;
        }

        if ($hotel->getHousingType()->getIsHaveRooms() && null === $value->getRoom()) {
            $this->context->buildViolation('Hotel not found')->atPath('hotel')->addViolation();
        }

        $reservationSubject = $hotel;

        if ($room) {
            $reservationSubject = $room;
        }

        $this->validateCountGuests($value, $reservationSubject);
        $this->validateDuration($value, $reservationSubject);
        $this->validateDatesAndPrice($value);
    }

    public function validateCountGuests(HotelsReservations $data, $reservationSubject)
    {
        if ($data->getAdults() > $reservationSubject->getCountMaxGuests()) {
            $this->context->buildViolation(
                sprintf(
                    'Max count guests for this %s is %d! Asset is %d',
                    $reservationSubject instanceof Hotels ? 'hotel' : 'room',
                    $reservationSubject->getCountMaxGuests(),
                    $data->getGuests()->count()
                )
            )->atPath('hotel')->addViolation();
        }
    }

    public function validateDuration(HotelsReservations $data, $reservationSubject)
    {
        $dateEnd = $data->getDateEnd();
        $dateStart = $data->getDateStart();

        if(!$dateEnd instanceof \DateTime || !$dateStart instanceof \DateTime){
            $this->context->buildViolation(
                sprintf(
                    'Date is not valid!'
                )
            )->addViolation();
        }
        else {
            $diff = $data->getDateEnd()->diff($data->getDateStart());

            if (($diff->format('%a') + 1) < $reservationSubject->getReservationMinDays()) {
                $this->context->buildViolation(
                    sprintf(
                        'Min days reservation for this %s is %d! Asset is %d',
                        $reservationSubject instanceof Hotels ? 'hotel' : 'room',
                        $reservationSubject->getReservationMinDays(),
                        $diff->format('a')
                    )
                )->addViolation();
            }
        }
    }

    public function validateDatesAndPrice(HotelsReservations $data)
    {
        $priceCriteria = new PriceCriteria();
        $priceCriteria
            ->setHotel($data->getHotel())
            ->setGuests($data->getAdults())
            ->addRoom($data->getRoom())
            ->setDateStart($data->getDateStart())
            ->setDateEnd($data->getDateEnd());

        $reservationRequest = new ReservationCriteria();
        $reservationRequest
            ->setHotel($data->getHotel())
            ->setDateStart($data->getDateStart())
            ->setDateEnd($data->getDateEnd())
            ->addRoom($data->getRoom());


        $reservations = $this->reservationService->getReservations($reservationRequest);
        $prices = $this->pricesService->getPrices($priceCriteria);

        if (!$prices) {
            $this->context->buildViolation('Reservation is impossible! Not prices')->atPath('hotel')->addViolation();
        }

        $sumPrice = 0;
        /** @var HotelsPrices $price */
        foreach ($prices as $price) {
            $date = $price->getDate();
            $sumPrice = $sumPrice + $price->getPrice();

            $countOfDay = $price->getCount();

            array_walk($reservations, function($reservation) use ($date, &$countOfDay){
                if($reservation->getDateStart() <= $date && $reservation->getDateEnd() >= $date){
                    $countOfDay = $countOfDay - 1;
                }
            });

            if ($data->getId()) {
                $countOfDay = $countOfDay + 1;
            }

            if ((int)$countOfDay <= 0) {
                $this->context->buildViolation('Reservations for the period can not be!')->atPath('hotel')->addViolation();
                return;
            }
        }

        if ($sumPrice != $data->getPrice()) {
            $this->context->buildViolation('Price is not valid!')->addViolation();
        }
    }
}