<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 03.04.16
 * Time: 15:17
 */

namespace CMS\HotelsBundle\Validator;


use CMS\HotelsBundle\Services\HotelsPricesRepository;
use CMS\HotelsBundle\Services\HotelsService;
use CMS\HotelsBundle\Services\ReservationsRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class HotelValidator extends ConstraintValidator
{
    /**
     * @var HotelsService
     */
    private $hotelsService;

    /**
     * HotelValidator constructor.
     * @param HotelsService $hotelsService
     */
    public function __construct(HotelsService $hotelsService)
    {
        $this->hotelsService = $hotelsService;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        $hotel = $this->hotelsService->getHotelById($value);

        if(!$hotel){
            $this->context->buildViolation(
                sprintf($constraint->message, $value)
            )->atPath('hotel')->addViolation();
        }
    }

}