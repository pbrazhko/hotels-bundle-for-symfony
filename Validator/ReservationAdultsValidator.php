<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 03.04.16
 * Time: 15:17
 */

namespace CMS\HotelsBundle\Validator;


use CMS\HotelsBundle\Entity\HotelsRooms;
use CMS\HotelsBundle\Services\HotelsRoomsService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ReservationAdultsValidator extends ConstraintValidator
{
    /**
     * @var HotelsRoomsService
     */
    private $roomsService;

    /**
     * ReservationAdultsValidator constructor.
     * @param HotelsRoomsService $roomsService
     */
    public function __construct(HotelsRoomsService $roomsService)
    {
        $this->roomsService = $roomsService;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        $form = $this->context->getRoot();
        /** @var HotelsRooms $room */
        $room = $form->get($constraint->roomPath)->getData();

        if(!$room instanceof HotelsRooms){
            $room = $this->roomsService->getRoomById($room);
        }

        if($room->getCountGuests() > $value || $room->getCountMaxGuests() < $value){
            $this->context->buildViolation(
                sprintf($constraint->message, $value)
            )->atPath('adults')->addViolation();
        }
    }

}