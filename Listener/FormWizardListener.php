<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 16.11.15
 * Time: 12:53
 */

namespace CMS\HotelsBundle\Listener;


use CMS\FormWizardBundle\Event\FormWizardEvent;
use CMS\GalleryBundle\Entity\Images;
use CMS\GeoBundle\Services\GeoMapsService;
use CMS\HotelsBundle\Entity\Hotels;
use CMS\HotelsBundle\Entity\HotelsRooms;
use CMS\HotelsBundle\Services\HotelsRoomsService;
use CMS\HotelsBundle\Services\HotelsService;

class FormWizardListener
{
    /**
     * @var GeoMapsService
     */
    private $geoMapsService;

    /**
     * FormWizardListener constructor.
     * @param $geoMapsService
     */
    public function __construct($geoMapsService)
    {
        $this->geoMapsService = $geoMapsService;
    }

    public function onWizardFlush(FormWizardEvent $event)
    {
        $wizard = $event->getWizard();

        if ($wizard->getName() !== 'hotels') {
            return;
        }

        $storage = $wizard->getStorage();

        /** @var Hotels $hotel */
        $hotel = $storage->getData(Hotels::class);

        if($hotel->getHousingType()->getIsHaveRooms()){
            return;
        }

        /** @var HotelsRooms $room */
        $room = $storage->getData(HotelsRooms::class);

        $room->setTitle($hotel->getTitle());
        $room->setPhotos($hotel->getPhotos());
        $room->setHotel($hotel);
        $room->setIsPublished($hotel->getIsPublished());
        $room->setIsDeleted($hotel->getIsDeleted());
        $room->setStatus(HotelsRoomsService::HOTELS_ROOMS_STATUS_COMPLETED);

        $hotel->setStatus(HotelsService::HOTELS_STATUS_COMPLETED);
        $hotel->addRoom($room);

        $storage->setData(Hotels::class, $hotel);
        $storage->setData(HotelsRooms::class, $room);
    }

    public function onBuildForm(FormWizardEvent $event)
    {
        $wizard = $event->getWizard();
        $stepName = $event->getStepName();

        if ($stepName == 'location') {
            $step = $wizard->getStep($stepName);

            $step->addVariable('map_token', $this->geoMapsService->createToken());
        }
    }
}