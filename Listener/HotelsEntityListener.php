<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 03.12.15
 * Time: 13:50
 */

namespace CMS\HotelsBundle\Listener;


use CMS\HotelsBundle\Entity\Hotels;
use CMS\HotelsBundle\Entity\HotelsRooms;
use CMS\HotelsBundle\Provider\ElasticaHotelsProvider;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Elastica\Type;

class HotelsEntityListener
{
    /**
     * @var ElasticaHotelsProvider
     */
    private $elasticaHotelsProvider;

    /**
     * HotelsEntityListener constructor.
     * @param ElasticaHotelsProvider $elasticaHotelsProvider
     * @internal param $hotelsElasticaType
     */
    public function __construct(ElasticaHotelsProvider $elasticaHotelsProvider)
    {
        $this->elasticaHotelsProvider = $elasticaHotelsProvider;
    }

    public function postPersist(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        if (!$entity instanceof Hotels && !$entity instanceof HotelsRooms) {
            return;
        }

        $this->elasticaHotelsProvider->addHotelToIndex(
            $entity instanceof Hotels ? $entity->getId() : $entity->getHotel()->getId()
        );
    }

    public function postUpdate(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        if (!$entity instanceof Hotels && !$entity instanceof HotelsRooms) {
            return;
        }

        $this->elasticaHotelsProvider->updateHotelIndex(
            $entity instanceof Hotels ? $entity->getId() : $entity->getHotel()->getId()
        );
    }
}