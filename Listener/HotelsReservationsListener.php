<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 10.04.16
 * Time: 20:25
 */

namespace CMS\HotelsBundle\Listener;


use CMS\HotelsBundle\Entity\HotelsReservations;
use CMS\HotelsBundle\Services\ReservationsService;
use Doctrine\ORM\Event\LifecycleEventArgs;

class HotelsReservationsListener
{
    /**
     * @var ReservationsService
     */
    private $reservationsService;

    /**
     * HotelsReservationsListener constructor.
     * @param ReservationsService $reservationsService
     */
    public function __construct(ReservationsService $reservationsService)
    {
        $this->reservationsService = $reservationsService;
    }

    public function prePersist(LifecycleEventArgs $event){
        $entity = $event->getEntity();

        if(!$entity instanceof HotelsReservations){
            return;
        }

        $this->setNightsReservation($entity);
    }

    public function preUpdate(LifecycleEventArgs $event){
        $entity = $event->getEntity();

        if(!$entity instanceof HotelsReservations){
            return;
        }

        $this->setNightsReservation($entity);
    }

    private function setNightsReservation(HotelsReservations $hotelsReservations){
        $hotelsReservations->setNights($this->reservationsService->calculateNights($hotelsReservations));
    }
}