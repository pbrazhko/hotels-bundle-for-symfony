<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 17.08.15
 * Time: 17:05
 */

namespace CMS\HotelsBundle\Listener;


use CMS\HotelsBundle\Entity\HotelsRooms;
use CMS\HotelsBundle\Services\HotelsRoomsService;
use Doctrine\ORM\Event\LifecycleEventArgs;

class HotelsRoomsEntityListener
{
    private $hotelsRoomsService;

    public function __construct(HotelsRoomsService $hotelsRoomsService)
    {
        $this->hotelsRoomsService = $hotelsRoomsService;
    }

    public function preUpdate(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        if (!$entity instanceof HotelsRooms) {
            return;
        }

        $hotel = $entity->getHotel();

        if (!$hotel->getHousingType()->getIsHaveRooms()) {
            $entity
                ->setIsPublished($hotel->getIsPublished())
                ->setIsDeleted($hotel->getIsDeleted());
        }
    }

    public function postPersist(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        if (!$entity instanceof HotelsRooms) {
            return;
        }

        if ($entity->getStatus() == HotelsRoomsService::HOTELS_ROOMS_STATUS_COMPLETED) {
            $this->hotelsRoomsService->addRoomPrice($entity->getHotel(), $entity);
        }
    }

    public function postUpdate(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        if (!$entity instanceof HotelsRooms) {
            return;
        }

        if ($entity->getStatus() == HotelsRoomsService::HOTELS_ROOMS_STATUS_COMPLETED) {
            $em = $event->getEntityManager();
            $unitOfWork = $em->getUnitOfWork();
            $unitOfWork->computeChangeSets();

            $diff = $unitOfWork->getEntityChangeSet($entity);

            if ($this->hotelsRoomsService->isNeedUpdatePrices($diff)) {
                $this->hotelsRoomsService->updateRoomPrices($entity, $entity->getHotel(), $diff);
            }
        }
    }
}