<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 04.09.15
 * Time: 22:47
 */

namespace CMS\HotelsBundle;


use CMS\HotelsBundle\Entity\Hotels;
use Doctrine\Common\Collections\ArrayCollection;

class HotelCriteria extends Hotels
{
    protected $is_published = null;

    protected $is_deleted = null;

    protected $isSelectLocation = false;

    protected $isSelectRooms = false;

    protected $isSelectPhotos = false;

    protected $isSelectServices = false;

    protected $isOneResult = false;

    /**
     * @param mixed $id
     * @return HotelCriteria
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param ArrayCollection $rooms
     * @return $this
     */
    public function setRooms($rooms)
    {
        return parent::setRooms(new ArrayCollection($rooms));
    }

    /**
     * @return ArrayCollection
     */
    public function getRooms()
    {
        return parent::getRooms()->toArray();
    }

    public function addRoom($room)
    {
        $this->rooms->add($room);

        return $this;
    }


    /**
     * @return boolean
     */
    public function getIsSelectLocation()
    {
        return $this->isSelectLocation;
    }

    /**
     * @param boolean $isSelectLocation
     * @return $this
     */
    public function setIsSelectLocation($isSelectLocation)
    {
        $this->isSelectLocation = $isSelectLocation;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSelectRooms()
    {
        return $this->isSelectRooms;
    }

    /**
     * @param boolean $isSelectRooms
     *
     * @return HotelCriteria
     */
    public function setIsSelectRooms($isSelectRooms)
    {
        $this->isSelectRooms = $isSelectRooms;
        
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSelectPhotos()
    {
        return $this->isSelectPhotos;
    }

    /**
     * @param boolean $isSelectPhotos
     *
     * @return HotelCriteria
     */
    public function setIsSelectPhotos($isSelectPhotos)
    {
        $this->isSelectPhotos = $isSelectPhotos;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSelectServices()
    {
        return $this->isSelectServices;
    }

    /**
     * @param boolean $isSelectServices
     * @return $this
     */
    public function setIsSelectServices($isSelectServices)
    {
        $this->isSelectServices = $isSelectServices;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsOneResult()
    {
        return $this->isOneResult;
    }

    /**
     * @param boolean $isOneResult
     * @return $this
     */
    public function setIsOneResult($isOneResult)
    {
        $this->isOneResult = $isOneResult;

        return $this;
    }
}