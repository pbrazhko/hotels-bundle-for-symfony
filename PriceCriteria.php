<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 04.09.15
 * Time: 21:48
 */

namespace CMS\HotelsBundle;


use CMS\HotelsBundle\Entity\Hotels;
use CMS\HotelsBundle\Entity\HotelsRooms;

class PriceCriteria extends AbstractCriteria
{
    private $hotel;

    private $rooms = array();

    private $guests;

    private $dateStart;

    private $dateEnd;

    /**
     * @return mixed
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * @param mixed $hotel
     * @return $this
     */
    public function setHotel($hotel)
    {
        if ($hotel instanceof Hotels) {
            $hotel = $hotel->getId();
        }

        $this->hotel = $hotel;

        return $this;
    }

    /**
     * @return array
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @param $room
     *
     * @return $this
     */
    public function addRoom($room)
    {
        if ($room instanceof HotelsRooms) {
            $room = $room->getId();
        }

        if (!in_array($room, $this->rooms) && null !== $room) {
            array_push($this->rooms, $room);
        }

        return $this;
    }

    /**
     * @param array $rooms
     * @return $this
     */
    public function setRooms(array $rooms)
    {
        foreach ($rooms as $room) {
            if ($room instanceof HotelsRooms) {
                $room = $room->getId();
            }

            if (!in_array($room, $this->rooms)) {
                array_push($this->rooms, $room);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGuests()
    {
        return $this->guests;
    }

    /**
     * @param mixed $guests
     *
     * @return $this
     */
    public function setGuests($guests)
    {
        $this->guests = $guests;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * @param mixed $dateStart
     *
     * @return $this
     */
    public function setDateStart($dateStart)
    {
        if (is_string($dateStart)) {
            $dateStart = new \DateTime($dateStart);
        }

        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @param mixed $dateEnd
     *
     * @return $this
     */
    public function setDateEnd($dateEnd)
    {
        if (is_string($dateEnd)) {
            $dateEnd = new \DateTime($dateEnd);
        }

        $this->dateEnd = $dateEnd;

        return $this;
    }
}