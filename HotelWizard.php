<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 17.11.15
 * Time: 16:55
 */

namespace CMS\HotelsBundle;


use CMS\HotelsBundle\Entity\Hotels;
use CMS\HotelsBundle\Entity\HotelsRooms;

class HotelWizard
{
    public static function config()
    {
        return array(
            'main' => array(
                'type' => 'CMS\HotelsBundle\Form\HotelWizard\HotelMainType',
                'service' => 'cms.hotels.service'
            ),
            'rooms' => array(
                'type' => 'CMS\HotelsBundle\Form\HotelWizard\HotelRoomsType',
                'service' => 'cms.hotels.rooms.service',
                'condition' => function (Hotels $hotel) {
                    return !$hotel->getHousingType()->getIsHaveRooms();
                },
                'expression' => function (Hotels $hotel, $data) {
                    if ($data instanceof HotelsRooms && null === $data->getTitle()) {
                        $data->setTitle($hotel->getTitle());
                    }
                }
            ),
            'placement' => array(
                'type' => 'CMS\HotelsBundle\Form\HotelWizard\HotelPlacementType',
                'service' => 'cms.hotels.rooms.service',
                'condition' => function (Hotels $hotel) {
                    return !$hotel->getHousingType()->getIsHaveRooms();
                },
                'expression' => function (Hotels $hotel, $data) {
                    if ($data instanceof HotelsRooms && null === $data->getTitle()) {
                        $data->setTitle($hotel->getTitle());
                    }
                }
            ),
            'price' => array(
                'type' => 'CMS\HotelsBundle\Form\HotelWizard\HotelPriceType',
                'service' => 'cms.hotels.rooms.service',
                'condition' => function (Hotels $hotel) {
                    return !$hotel->getHousingType()->getIsHaveRooms();
                },
                'expression' => function (Hotels $hotel, $data) {
                    if ($data instanceof HotelsRooms && null === $data->getTitle()) {
                        $data->setTitle($hotel->getTitle());
                    }
                }
            ),
            'reservation' => array(
                'type' => 'CMS\HotelsBundle\Form\HotelWizard\HotelReservationType',
                'service' => 'cms.hotels.rooms.service',
                'condition' => function (Hotels $hotel) {
                    return !$hotel->getHousingType()->getIsHaveRooms();
                },
                'expression' => function (Hotels $hotel, $data) {
                    if ($data instanceof HotelsRooms && null === $data->getTitle()) {
                        $data->setTitle($hotel->getTitle());
                    }
                }
            ),
            'services' => array(
                'type' => 'CMS\HotelsBundle\Form\HotelWizard\HotelServicesType',
                'service' => 'cms.hotels.rooms.service',
                'condition' => function (Hotels $hotel) {
                    return !$hotel->getHousingType()->getIsHaveRooms();
                },
                'expression' => function (Hotels $hotel, $data) {
                    if ($data instanceof HotelsRooms && null === $data->getTitle()) {
                        $data->setTitle($hotel->getTitle());
                    }
                }
            ),
            'photos' => array(
                'type' => 'CMS\HotelsBundle\Form\HotelWizard\HotelPhotosType',
                'service' => 'cms.hotels.rooms.service'
            )
        );
    }

    public static function getNextStepName($stepName, $hotel)
    {
        $steps = self::config();
        $stepNames = array_keys($steps);

        foreach ($stepNames as $name) {
            if ($stepName == $name) {
                $nextStepName = next($stepNames);
                $nextStep = $steps[$nextStepName];

                if (isset($nextStep['condition']) && !call_user_func($nextStep['condition'], $hotel)) {
                    return self::getNextStepName($nextStepName, $hotel);
                }

                return $nextStepName;
            }
        }

        return reset(array_keys($steps));
    }

    public static function getFirstStepName()
    {
        $steps = array_keys(self::config());

        return reset($steps);
    }
}