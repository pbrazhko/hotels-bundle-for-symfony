<?php

namespace CMS\HotelsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class HotelsBundle extends Bundle
{
    public function isEnabled()
    {
        return true;
    }

    public function getDescription()
    {
        return array(
            array(
                'title' => 'Hotels',
                'defaultRoute' => 'cms_hotels_list'
            ),
            array(
                'title' => 'Services',
                'defaultRoute' => 'cms_hotels_services_list'
            ),
            array(
                'title' => 'Housing types',
                'defaultRoute' => 'cms_hotels_housing_types_list'
            ),
            array(
                'title' => 'Reservations',
                'defaultRoute' => 'cms_hotels_reservations_list'
            ),
        );
    }
}
