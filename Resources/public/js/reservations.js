/**
 * Created by pavel on 14.03.15.
 */
(
    function ($){
        $.Reserv = function (config) {
            return $.Reserv.impl.init(config);
        };

        $.Reserv.config = {
            hotelSelector: null,
            roomSelector: null,
            dateStartSelector: null,
            dateEndSelector: null,
            calendarSelector: null,
            guestsSelector: null,
            guestSelector: null,
            priceSelector: null,
            priceContainer: null,
            hotelInfoRoute: null,
            getPriceRoute: null,
            showingMonth: 2,
            dateFormat: 'dd.mm.yy',
            prototype: null,
            reservationSubmit: null
        };

        $.Reserv.impl = {
            config: null,
            dates: [],
            hotel: null,
            room: null,
            guestsCount: 0,
            isRequiredRooms: false,
            init: function (config) {
                this.config = $.extend({}, $.Reserv.config, config);

                this.initCalendar();
                this.initGuests();

                this.bind();

                if (this.config.hotelSelector == undefined || this.config.hotelSelector.val() == '') {
                    this.showMessage('System error!', 'Hotel Id is not defined!');
                    return false;
                }

                if (this.config.roomSelector == undefined || this.config.roomSelector.val() == '') {
                    this.showMessage('System error!', 'Hotel room Id is not defined!');
                    return false;
                }


                $.Reserv.impl.loadInfo(this.config.hotelSelector.val(), this.config.roomSelector.val());
            },
            initCalendar: function(){
                var self = this;

                $(this.config.calendarSelector).datepicker({
                    nextText: '',
                    prevText: '',
                    showOtherMonths: true,
                    dateFormat: self.config.dateFormat,
                    numberOfMonths: self.config.showingMonth,
                    beforeShowDay: function (date) {
                        var dataStart = $.datepicker.parseDate(self.config.dateFormat, self.config.dateStartSelector.val());
                        var dataEnd = $.datepicker.parseDate(self.config.dateFormat, self.config.dateEndSelector.val());
                        var now = new Date();
                        var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

                        var isHasHotel = $.Reserv.impl.hotel != null;

                        return [
                            isHasHotel && date.valueOf() >= today.valueOf() && self.checkDate(date),
                            dataStart && ((date.getTime() == dataStart.getTime()) || (dataEnd && date >= dataStart && date <= dataEnd)) ? "ui-state-range" : ""
                        ];
                    },
                    onSelect: function (date, inst) {
                        var dataStart = $.datepicker.parseDate(self.config.dateFormat, self.config.dateStartSelector.val());
                        var dataEnd = $.datepicker.parseDate(self.config.dateFormat, self.config.dateEndSelector.val());
                        var selectedDate = $.datepicker.parseDate(self.config.dateFormat, date);

                        if (!dataStart || dataEnd) {
                            self.config.dateStartSelector.val(date);
                            self.config.dateEndSelector.val("");
                        } else if (selectedDate < dataStart) {
                            self.config.dateEndSelector.val(self.config.dateStartSelector.val());
                            self.config.dateStartSelector.val(date);
                        } else {
                            self.config.dateEndSelector.val(date);
                        }

                        //$(this).datepicker();

                        self.refreshPrice();
                    }
                });
            },
            checkDate: function (date) {
                var status = true;
                var allowedDates = this.dates || {};
                var selectedStartDate = $(this.config.dateStartSelector).val();
                var selectedEndDate = $(this.config.dateEndSelector).val();

                var stringDate = $.datepicker.formatDate('dd.mm.yy', date);

                if (!allowedDates.hasOwnProperty(stringDate) || allowedDates[stringDate] <= 0) {
                    status = false;
                }

                var isNotEmptySelectedStartDate = selectedStartDate != undefined && selectedStartDate != '';
                var isMotEmptySelectedEndDate = selectedEndDate != undefined && selectedEndDate != '';

                if (isNotEmptySelectedStartDate && isMotEmptySelectedEndDate) {
                    selectedStartDate = $.datepicker.parseDate(this.config.dateFormat, selectedStartDate);
                    selectedEndDate = $.datepicker.parseDate(this.config.dateFormat, selectedEndDate);

                    if (date.valueOf() >= selectedStartDate.valueOf() && date.valueOf() <= selectedEndDate.valueOf()) {
                        status = true;
                    }
                }

                return status;
            },
            initGuests: function(){
                this.config.prototype = this.config.guestsSelector.find('.prototype').data('prototype');
                this.guestsCount = this.config.guestSelector.length;

                this.config.guestsSelector.find('.datepicker').datepicker({
                    yearRange: "-70:+0",
                    nextText: '',
                    prevText: '',
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: this.config.dateFormat
                });

                var self = this;
                this.config.guestsSelector.find('.add').on('click', function(event){
                    event.preventDefault();

                    if ($(self.room).prop('count_max_guests') == undefined) {
                        self.showMessage('Info!', 'Room information is not loaded!');
                        return;
                    }

                    if ($(self.room).prop('count_max_guests') < self.guestsCount + 1) {
                        self.showMessage('Info!', 'Max count guests in this hotel is ' + $(self.room).prop('count_max_guests'));
                        return;
                    }

                    self.guestsCount = self.guestsCount + 1;

                    var newGuest = $.parseHTML(self.config.prototype.replace(/__name__/g, self.guestsCount));

                    $(newGuest).find('.datepicker').datepicker({dateFormat: self.config.dateFormat});

                    self.config.guestsSelector.find('.prototype').append(newGuest);

                    $(newGuest).find('.close').on('click', function (event) {
                        event.preventDefault();

                        $(this).closest('.reservations_guest').hide("puff", function () {
                            $(this).remove();
                        });

                        self.guestsCount = self.guestsCount - 1;

                        self.refreshPrice();
                    });

                    self.refreshPrice();
                });

                this.config.guestsSelector.find('.close').on('click', function (event) {
                    event.preventDefault();

                    $(this).closest('.reservations_guest').hide("puff", function () {
                        $(this).remove();
                    });

                    self.guestsCount = self.guestsCount - 1;

                    self.refreshPrice();
                });
            },
            refreshGuests: function () {
                if (this.guestsCount >= this.room.count_guests) return;

                for (var i = this.guestsCount; i < this.room.count_guests; i++) {
                    this.config.guestsSelector.find('.add').click();
                }
            },
            getRoom: function (hotel) {
                var roomId = this.config.roomSelector.val();

                if (roomId == undefined || roomId == null) {
                    throw new Error('This hotel is not have reservation!');
                }

                if (!hotel.rooms || hotel.rooms.length < 1) {
                    throw  new Error('The rooms in hotel is not found!');
                }

                for (var index in hotel.rooms) {
                    var room = hotel.rooms[index];

                    if (room.id == roomId) return room;
                }

                return null;
            },
            bind: function () {
                this.config.hotelSelector.on('change', this.changeHotel);
                this.config.hotelSelector.closest('form').find('input[type="submit"]').on('click', this.reservation);
            },
            reservation: function (e){
                var formData = $(this).closest('form').serializeArray();

                var result = $.Reserv.impl.validateForm(formData);
                if(true !== result){
                    $.Reserv.impl.showMessage('Info!', result);
                    e.preventDefault();

                }
            },
            validateForm: function (data){
                if (data.length < 1) return 'Fields is empty';

                for(var i in data) {
                    var field = data[i];

                    if (field.name == this.config.hotelSelector.attr('name')
                        && (field.value == undefined || field.value == '')) {
                        return 'Hotel not selected!';
                    }
                    else if (field.name == this.config.roomSelector.attr('name')
                        && this.isRequiredRooms && (field.value == undefined || field.value == '')) {
                        return 'Room is required!';
                    }
                    else if (field.name == this.config.dateStartSelector.attr('name')
                        && (field.value == undefined || field.value == '')) {
                        return 'Date reservation is required!';
                    }
                    else if (field.name == this.config.dateEndSelector.attr('name')
                        && (field.value == undefined || field.value == '')) {
                        return 'Date reservation is required!';
                    }
                }

                return true;
            },
            loadInfo: function (id, room) {
                if (this.config.hotelInfoRoute == null) {
                    throw new Error('Hotel information url is nor defined');
                }

                var params = {
                    hotel: id,
                    room: room
                };

                var url = Routing.generate(
                    this.config.hotelInfoRoute,
                    params
                );

                var self = this;
                var callback = function (data) {
                    if (data.hotel == undefined || data.dates == undefined) {
                        var message = 'Sorry, the system is not available!<br/><br/>' +
                            '<a href="/">' + Translator.trans('Return to main page', {}, 'hotels') + '</a>';

                        self.showMessage('System error!', message, false);
                    }

                    self.dates = data.dates;
                    self.hotel = data.hotel;
                    self.room = self.getRoom(self.hotel);

                    self.config.calendarSelector.datepicker('refresh');
                    self.refreshGuests();
                };

                this.send(url, 'POST', {}, callback);
            },
            refreshPrice: function(){
                var formData = $(this.config.hotelSelector).closest('form').serializeArray();
                var result = $.Reserv.impl.validateForm(formData);

                if (true !== result || this.guestsCount == 0) return;

                var url = Routing.generate(
                    this.config.getPriceRoute
                );

                var data = {
                    'hotel': this.hotel.id,
                    'date_start': this.config.dateStartSelector.val(),
                    'date_end': this.config.dateEndSelector.val(),
                    'guests': this.guestsCount,
                    'room': this.config.roomSelector.val()
                };

                var self = this;
                var callback = function(data){
                    $(self.config.priceContainer).removeClass('active');

                    if(data.price > 0){
                        $(self.config.priceContainer).addClass('active');
                    }

                    self.config.priceContainer.html(data.price.toFixed(2) + ' &euro;');
                    self.config.priceSelector.val(data.price);
                };

                this.send(url, 'POST', data, callback);
            },
            showMessage: function (title, message, isClosing) {
                if ($.isFunction($.modal)) {
                    isClosing = isClosing == undefined ? true : isClosing;

                    $.modal({
                        'title': title,
                        'showClose': isClosing,
                        'content': message
                    });

                    return;
                }

                console.log(message);
            },
            send: function (url, method, data, callback) {
                $.ajax({
                    method: method,
                    url: url,
                    data: data
                })
                .success(function (data) {
                    if ($.isFunction(callback)) {
                        callback(data);
                    }
                })
                .fail(function (jqXHR, textStatus) {
                    console.error("Request failed: " + textStatus);
                })
            }
        }
    }
)(jQuery);