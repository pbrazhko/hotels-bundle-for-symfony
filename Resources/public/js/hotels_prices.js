/**
 * Created by pavel on 27.07.15.
 */

(function ($) {
    var hotelsPrices = {
        config: {
            'enabledControlPanel': true,
            'enabledChangeCountButton': true,
            'enabledChangePriceButton': true,
            'enabledStopSalesButton': true,
            'enabledStartSalesButton': true,
            'controlsSelector': '> .control-panel',
            'stopSalesRoute': null,
            'startSalesRoute': null,
            'changeCountRoute': null,
            'changePriceRoute': null
        },
        impl: {
            config: {},
            controlPanel: null,
            target: null,
            table: null,
            init: function (target, config) {
                this.target = target
                this.table = $(target).find('table');
                this.controlPanel = $(target).find('.control-panel')
                this.config = $.extend({}, hotelsPrices.config, config);

                if (!$.isFunction($.fn.selectable)) {
                    throw new Error('Selectable is not defined');
                }

                if(!$.isFunction($.modal)){
                    throw new Error('Modal is not defined');
                }

                if (this.config.enabledControlPanel) {
                    this.initControlPanel();
                }

                this.bind();
            },
            initControlPanel: function () {
                var controlContainer = $(this.controlPanel).find(this.config.controlsSelector);
                if (controlContainer == undefined) {
                    return;
                }

                this.addChangePriceButton(controlContainer);
                this.addChangeCountButton(controlContainer);
                this.addStopSalesButton(controlContainer);
                this.addStartSalesButton(controlContainer);
            },
            addChangePriceButton: function(controlContainer){
                if(!this.config.enabledChangePriceButton) return;

                var button = this.createButtonNode('change_price', Translator.trans('Change price', {}, 'hotels'));

                this.setEventForButton(button, 'click', function(event){
                    var self = hotelsPrices.impl;
                    var selectedCells = hotelsPrices.impl.getSelectedCells();

                    if(selectedCells.length < 1){
                        throw new Error('Before selecting cells!');
                    }

                    var form = self.buildChangePriceForm();
                    self.showModal(
                        Translator.trans('Change price', {}, 'hotels'),
                        form
                    );

                    $(form).on("submit", function (e){
                        e.preventDefault();

                        self.changePrice(this, selectedCells);
                    });
                });

                return $(controlContainer).append(button);
            },
            addChangeCountButton: function(controlContainer){
                if(!this.config.enabledChangeCountButton) return;

                var button = this.createButtonNode('change_count', Translator.trans('Change count', {}, 'hotels'));

                this.setEventForButton(button, 'click', function(event){
                    var self = hotelsPrices.impl;
                    var selectedCells = self.getSelectedCells();

                    if(selectedCells.length < 1){
                        throw new Error('Before selecting cells!');
                    }

                    var form = self.buildChangeCountForm();
                    self.showModal(
                        Translator.trans('Change count', {}, 'hotels'),
                        form
                    );

                    $(form).on("submit", function (e) {
                        e.preventDefault();

                        self.changeCount(this, selectedCells);
                    });
                });

                return $(controlContainer).append(button);
            },
            addStopSalesButton: function(controlContainer){
                if(!this.config.enabledStopSalesButton) return;

                var button = this.createButtonNode('stop_sales', Translator.trans('Stop sales', {}, 'hotels'));

                this.setEventForButton(button, 'click', function(event){
                    var self = hotelsPrices.impl;
                    var selectedCells = hotelsPrices.impl.getSelectedCells();

                    if(selectedCells.length < 1){
                        throw new Error('Before selecting cells!');
                    }

                    var dates = [];
                    var hotel = null;
                    var room = null;

                    selectedCells.each(function (key, cell){
                        var cellData = $(cell).closest('td').data();

                        if (cellData.status != 1) return;

                        hotel = cellData.hotel;
                        dates.push(cellData.date);

                        if(cellData.room != undefined){
                            room = cellData.room;
                        }
                    });

                    var url = Routing.generate(self.config.stopSalesRoute);
                    var data = {
                        'hotel': hotel,
                        'room': room,
                        'dates': dates
                    };

                    var callback = function (data){
                        if(data.status != undefined && data.status == 'success'){
                            window.location.reload();
                        }
                    };

                    self.send(url, 'POST', data, callback);
                });

                return $(controlContainer).append(button);
            },
            addStartSalesButton: function (controlContainer) {
                if(!this.config.enabledStartSalesButton) return;

                var button = this.createButtonNode('start_sales', Translator.trans('Start sales', {}, 'hotels'));

                this.setEventForButton(button, 'click', function (event) {
                    var self = hotelsPrices.impl;
                    var selectedCells = hotelsPrices.impl.getSelectedCells();
                    if (selectedCells.length < 1) {
                        throw new Error('Before selecting cells!');
                    }

                    var dates = [];
                    var hotel = null;
                    var room = null;

                    selectedCells.each(function (key, cell) {
                        var cellData = $(cell).closest('td').data();

                        if (cellData.status == 1) return;

                        hotel = cellData.hotel;
                        dates.push(cellData.date);

                        if (cellData.room != undefined) {
                            room = cellData.room;
                        }
                    });

                    var url = Routing.generate(self.config.startSalesRoute);
                    var data = {
                        'hotel': hotel,
                        'room': room,
                        'dates': dates
                    };
                    var callback = function (data) {
                        if (data.status != undefined && data.status == 'success') {
                            window.location.reload();
                        }
                    };

                    self.send(url, 'POST', data, callback);
                });

                return $(controlContainer).append(button);
            },
            buildChangePriceForm: function () {
                var label = $('<label/>').text(Translator.trans('New price', {}, 'hotels'));
                var input = $('<input/>').attr({'type': 'text', 'name': 'price', 'value': ''});
                var submit = $('<input/>').attr({'type': 'submit', 'class': 'btn', 'value': Translator.trans('Save', {}, 'hotels')});
                var form = $('<form/>');

                form.append(label);
                form.append(input);
                form.append(submit);

                return form;
            },
            buildChangeCountForm: function () {
                var label = $('<label/>').text(Translator.trans('Enter count', {}, 'hotels'));
                var input = $('<input/>').attr({'type': 'text', 'name': 'count', 'value': ''});
                var submit = $('<input/>').attr({'type': 'submit', 'class': 'btn', 'value': Translator.trans('Save', {}, 'hotels')});
                var form = $('<form/>');

                form.append(label);
                form.append(input);
                form.append(submit);

                return form;
            },
            createButtonNode: function(classCss, text){
                return $('<button/>').attr({'type': 'button', 'class': classCss}).text(text);
            },
            setEventForButton: function(button, event, callback){
                return $(button).on(event, callback);
            },
            showModal: function(modalTitle, modalContent){
                $.modal({
                    'title': modalTitle,
                    'content': modalContent
                });
            },
            getSelectedCells: function(){
                return $(this.table).find('.ui-selected');
            },
            refreshControlButtonsStatus: function (selectedCells) {
                var statuses = {};
                statuses.changePriceButtonStatus = this.checkPriceButtonStatus(selectedCells);
                statuses.changeCountButtonStatus = this.checkCountButtonStatus(selectedCells);
                statuses.changeStopSalesButtonStatus = this.checkStopSalesButtonStatus(selectedCells);
                statuses.changeStartSalesButtonStatus = this.checkStartSalesButtonStatus(selectedCells);

                this.updateControlButtonsStatus(statuses);
            },
            updateControlButtonsStatus: function (statuses) {
                if (statuses.changePriceButtonStatus) {
                    $('button.change_price').addClass('active');
                }
                else {
                    $('button.change_price').removeClass('active');
                }

                if (statuses.changeCountButtonStatus) {
                    $('button.change_count').addClass('active');
                }
                else {
                    $('button.change_count').removeClass('active');
                }

                if (statuses.changeStopSalesButtonStatus) {
                    $('button.stop_sales').addClass('active');
                }
                else {
                    $('button.stop_sales').removeClass('active');
                }

                if (statuses.changeStartSalesButtonStatus) {
                    $('button.start_sales').addClass('active');
                }
                else {
                    $('button.start_sales').removeClass('active');
                }
            },
            checkPriceButtonStatus: function (selectedCells) {
                if(selectedCells == undefined || selectedCells.length <= 0){
                    return false;
                }

                return true;
            },
            checkCountButtonStatus: function (selectedCells) {
                if(selectedCells == undefined || selectedCells.length <= 0){
                    return false;
                }

                return true;
            },
            checkStopSalesButtonStatus: function (selectedCells) {
                var status = false;

                if (selectedCells != undefined && selectedCells.length > 0) {
                    $(selectedCells).each(function (k, cell) {
                        var data = $(cell).closest('td').data();

                        if (data.status == "1") {
                            status = true;
                        }
                    });
                }

                return status;
            },
            checkStartSalesButtonStatus: function (selectedCells) {
                var status = false;

                if (selectedCells != undefined && selectedCells.length > 0) {
                    $(selectedCells).each(function (k, cell) {
                        var data = $(cell).closest('td').data();

                        if (data.status != "1") {
                            status = true;
                        }
                    });
                }

                return status;
            },
            changePrice: function (form, selectedCells) {
                var dates = [];
                var hotel = null;
                var room = null;

                selectedCells.each(function (key, cell) {
                    var cellData = $(cell).closest('td').data();

                    if (cellData.status != 1) return;

                    hotel = cellData.hotel;
                    dates.push(cellData.date);

                    if (cellData.room != undefined) {
                        room = cellData.room;
                    }
                });

                var url = Routing.generate(this.config.changePriceRoute);
                var data = {
                    'hotel': hotel,
                    'room': room,
                    'dates': dates,
                    'price': $(form).find('input[name="price"]').val()
                };

                var callback = function (data) {
                    if (data.status != undefined && data.status == 'success') {
                        window.location.reload();
                    }
                };

                this.send(url, 'POST', data, callback);
            },
            changeCount: function(form, selectedCells){
                var dates = [];
                var hotel = null;
                var room = null;

                selectedCells.each(function (key, cell) {
                    var cellData = $(cell).closest('td').data();

                    if (cellData.status != 1) return;

                    hotel = cellData.hotel;
                    dates.push(cellData.date);

                    if (cellData.room != undefined) {
                        room = cellData.room;
                    }
                });

                var url = Routing.generate(this.config.changeCountRoute);
                var data = {
                    'hotel': hotel,
                    'room': room,
                    'dates': dates,
                    'count': $(form).find('input[name="count"]').val()
                };
                var callback = function (data) {
                    if (data.status != undefined && data.status == 'success') {
                        window.location.reload();
                    }
                };

                this.send(url, 'POST', data, callback);
            },
            send: function (url, method, data, callback) {
                $.ajax({
                    'url': url,
                    'data': data,
                    'method': method,
                    'success': callback
                });
            },
            bind: function () {
                $(document).on('selectedCells', function (event) {
                    hotelsPrices.impl.refreshControlButtonsStatus(hotelsPrices.impl.getSelectedCells());
                });

                $(this.table).selectable({
                    'filter': 'tr>td div.selectable',
                    selecting: function (event, ui) {
                        $(ui.selecting).addClass('select');
                    },
                    unselecting: function (event, ui) {
                        $(ui.unselecting).removeClass('select');
                    },
                    selected: function (event, ui) {
                        $(ui.selected).addClass('select');
                    },
                    unselected: function (event, ui) {
                        $(ui.unselected).removeClass('select');
                    },
                    stop: function (event, ui) {
                        $(document).trigger('selectedCells');
                    }
                });

                $('tr>td:not(:first-child).select').on('')
            }
        }
    };

    $.fn.hotelsPrices = function (config) {
        return hotelsPrices.impl.init(this, config);
    };
})(jQuery);