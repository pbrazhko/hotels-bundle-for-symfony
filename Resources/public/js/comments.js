/**
 * Created by pavel on 31.07.15.
 */
(function ($) {
    var commentsForm = {
        config: {
            'addCommentRoute': null
        },
        impl: {
            config: {},
            target: null,
            init: function (target, config) {
                this.target = target;
                this.config = $.extend({}, config, this.config);

                this.bind();
            },
            send: function(url, method, data, callback){
                $.ajax({
                    'url': url,
                    'data': data,
                    'method': method,
                    'success': callback
                });
            },
            bind: function () {
                var self = this;
                $(this.target).submit(function (event) {
                    event.preventDefault();

                    var url = Routing.generate(self.config.addCommentRoute);
                    var data = self.target.serializeArray();
                    var callback = function(data){
                        if (data.status != undefined && data.status == true) {
                            $(self.target).html('<div class="success">' + Translator.trans('hotels.comments.success', [], 'hotels') + '</div>');
                        }
                        else {
                            if (data.errors && data.errors.length > 0) {
                                for (var i in data.errors) {
                                    var error = data.errors[i];
                                    $(self.target).prepend($('<div/>').addClass('error').text(error));
                                }
                            }
                        }
                    };

                    return self.send(url, 'POST', data, callback);
                });
            }
        }
    };

    $.fn.commentsForm = function (config) {
        return commentsForm.impl.init(this, config);
    }
})(jQuery);