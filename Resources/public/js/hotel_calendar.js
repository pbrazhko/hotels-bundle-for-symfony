/**
 * Created by pavel on 20.08.15.
 */

(function ($) {
    var hotelsCalendar = {
        calendar: null,
        timers: null,
        config: {
            'filter': 'tr > td div.selectable',
            'hotelCalendarRoute': null,
            'hotelRoomCalendarRoute': null,
            'infoRoute': null
        },
        impl: {
            config: {},
            tooltips: {},
            init: function (calendar, config) {
                hotelsCalendar.calendar = calendar;
                this.config = $.extend({}, hotelsCalendar.config, config);
                this.bind();
            },
            loadInformation: function (target) {
                var self = this;
                var tooltip = $(target).find('.tooltip');
                if (tooltip.length > 0) {
                    var p = $(target).position();
                    tooltip.attr({
                        'class': 'tooltip',
                        'style': 'left: ' + p.left + 'px; top:' + (p.top + 21) + 'px;'
                    });
                    $(tooltip).show();
                    return;
                }

                var params = {};
                var data = $(target).closest('td').data();

                data.hotel != undefined ? params.hotel = data.hotel : null;
                data.date != undefined ? params.date = data.date : null;
                data.room != undefined ? params.room = data.room : null;

                var url = Routing.generate(this.config.infoRoute, params);
                var callback = function (data) {
                    var p = $(target).position();
                    tooltip = $('<div/>')
                        .attr({
                            'class': 'tooltip',
                            'style': 'left: ' + p.left + 'px; top:' + (p.top + 21) + 'px;'
                        })
                        .append(self.createMessage(data));

                    $(target).append(tooltip);
                };

                self.timers = setTimeout(function () {
                    self.send(url, 'GET', {}, callback);
                }, 1000);
            },
            createMessage: function (data) {
                var message = $('<div/>');

                if(data.hotel == undefined || data.reservations == undefined || data.prices == undefined){
                    return 'Build message is impossible';
                }

                if(data.hotel.housing_type.is_have_rooms) {
                    if (data.hotel.rooms != undefined && data.hotel.rooms.length > 0) {
                        message.html('<h4>Hotel: ' + data.hotel.title[Translator.locale.toLowerCase()] + '</h4></br>');
                        for (var i in data.hotel.rooms) {
                            var room = data.hotel.rooms[i];

                            var reservations = this.findReservation(data.reservations, data.hotel.id, room.id);
                            var prices = this.findPrices(data.prices, data.hotel.id, room.id);
                            var guestsMessage = this.createGuestsMessage(prices);
                            message
                                .append(
                                    $('<div/>').addClass('room')
                                        .append(
                                            $('<div/>')
                                                .append(
                                                    $('<a/>')
                                                        .attr('href', Routing.generate(this.config.hotelRoomCalendarRoute, {
                                                                'hotel': data.hotel.id,
                                                                'room': room.id
                                                            })
                                                        )
                                                        .click(function(e){e.stopPropagation();})
                                                        .text(room.title[Translator.locale.toLowerCase()])
                                                )
                                                .append(': ' + room.count + '/' + reservations.length + '</br>')
                                            )
                                        .append(guestsMessage)
                                    );
                        }
                    }
                }
                else{
                    var reservations = this.findReservation(data.reservations, data.hotel.id);
                    var prices = this.findPrices(data.prices, data.hotel.id);
                    var guestsMessage = this.createGuestsMessage(prices);
                    var room = data.hotel.rooms !== undefined || data.hotel.rooms.length > 0 ? data.hotel.rooms[0] : [];
                    message.append(
                        $('<div/>').addClass('room')
                            .append(
                                $('<div/>')
                                    .append(data.hotel.title[Translator.locale.toLowerCase()] + ': ' + room.count + '/' + reservations.length + '</br>')
                            )
                            .append(guestsMessage)
                    );
                }

                return message
            },
            createGuestsMessage: function (prices){
                var message = $('<ul/>').addClass('guests');

                if(prices != undefined && prices.length > 0){
                    for(var index in prices){
                        var price = prices[index];
                        message.append(
                            $('<li/>').html((Translator.trans('Guests', {}, 'hotels')) + ' ' + price.guests + ': ' + price.price + '&euro;')
                        );
                    }
                }

                return message;
            },
            findReservation: function (reservations, hotel, room) {
                var result = [];

                for (var i in reservations) {
                    var reservation = reservations[i];
                    if ((hotel != undefined && hotel != reservation.hotel))continue;
                    if (room != undefined && room != reservation.room) continue;

                    result.push(reservation);
                }

                return result;
            },
            findPrices: function(prices, hotel, room){
                var result = [];

                for (var i in prices) {
                    var price = prices[i];
                    if ((hotel != undefined && hotel != price.hotel))continue;
                    if (room != undefined && room != price.room) continue;

                    result.push(price);
                }

                return result;
            },
            send: function (url, method, data, callback) {
                $.ajax({
                    'url': url,
                    'data': data,
                    'method': method,
                    'success': callback
                });
            },
            bind: function () {
                var self = this;
                $(hotelsCalendar.calendar).find(self.config.filter).on('mouseover', function () {
                    self.loadInformation(this);
                });

                $(hotelsCalendar.calendar).find(self.config.filter).on('mouseout', function () {
                    clearTimeout(self.timers);
                    $(this).find('.tooltip').hide();
                });
            }
        }
    };

    $.fn.hotelsCalendar = function (config) {
        return hotelsCalendar.impl.init(this, config);
    };
})(jQuery);
