/**
 * Created by pavel on 14.03.15.
 */

$(document).ready(function () {
    var seasonsPrototype = $('#seasons').data('prototype');
    var seasonsIndex = $('#seasons').find('.season').length;

    function closeSeason(selector) {
        $(selector).closest('.season').hide("puff", function () {
            $(this).remove();
        });
    }

    $('#add_season').on('click', function (event) {
        seasonsIndex = seasonsIndex + 1;

        var newSeason = $.parseHTML(seasonsPrototype.replace(/__name__/g, seasonsIndex));

        $('#seasons').append(newSeason);

        $('#cms_hotelsbundle_hotels_seasons_' + seasonsIndex + '_date_start').datepicker({dateFormat: 'dd.mm'});
        $('#cms_hotelsbundle_hotels_seasons_' + seasonsIndex + '_date_end').datepicker({dateFormat: 'dd.mm'});

        $(newSeason).find('.close').on('click', function (event) {
            event.preventDefault();

            closeSeason(this);
        });
    });

    $('.season .close').on('click', function (event) {
        event.preventDefault();

        closeSeason(this);
    });
});