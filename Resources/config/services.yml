parameters:
    hotels.service.class: CMS\HotelsBundle\Services\HotelsService
    hotels.rooms.service.class: CMS\HotelsBundle\Services\HotelsRoomsService
    hotels.services.service.class: CMS\HotelsBundle\Services\ServicesService
    hotels.housing.types.service.class: CMS\HotelsBundle\Services\HousingTypesService
    hotels.seasons.service.class: CMS\HotelsBundle\Services\SeasonsService
    hotels.prices.service.class: CMS\HotelsBundle\Services\HotelsPricesService
    hotels.reservations.service.class: CMS\HotelsBundle\Services\ReservationsService
    hotels.search.provider.class: CMS\HotelsBundle\Provider\SearchHotelsProvider
    hotels.comments.class: CMS\HotelsBundle\Services\HotelsCommentsService

    # form types services class #
    hotels.form.type.seasons.class: CMS\HotelsBundle\Form\Types\SeasonsType
    hotels.form.type.season.class: CMS\HotelsBundle\Form\Types\SeasonType
    hotels.form.type.reservations_guests.class: CMS\HotelsBundle\Form\Types\ReservationsGuestsType
    hotels.form.type.reservations_guest.class: CMS\HotelsBundle\Form\Types\ReservationsGuestType

    # twig extensions class #
    hotels.twig.class: CMS\HotelsBundle\Twig\HotelsExtension
    hotels.reservations.twig.class: CMS\HotelsBundle\Twig\HotelsReservationsExtension

    # listeners class #
    hotels.listener.class: CMS\HotelsBundle\Listener\HotelsEntityListener
    hotels.rooms.listener.class: CMS\HotelsBundle\Listener\HotelsRoomsEntityListener
    hotels.wizard.listener.class: CMS\HotelsBundle\Listener\FormWizardListener
    hotels.reservations.listener.class: CMS\HotelsBundle\Listener\HotelsReservationsListener

    # validators
    hotels.reservation.validator.class: CMS\HotelsBundle\Validator\ReservationValidator
    hotels.reservation.date_start.validator.class: CMS\HotelsBundle\Validator\ReservationDateStartValidator
    hotels.reservation.date_end.validator.class: CMS\HotelsBundle\Validator\ReservationDateEndValidator
    hotels.reservation.adults.validator.class: CMS\HotelsBundle\Validator\ReservationAdultsValidator
    hotels.validator.class: CMS\HotelsBundle\Validator\HotelValidator
    hotels.room.validator.class: CMS\HotelsBundle\Validator\RoomValidator

    # security voters #
    hotels.security.voter.class: CMS\HotelsBundle\Security\Voter\HotelsVoter

    # provider class #
    hotels.geo.provider.class: CMS\HotelsBundle\Provider\GeoHotelsProvider
    hotels.elastica.hotels.provider.class: CMS\HotelsBundle\Provider\ElasticaHotelsProvider

    # transformer class #
    hotels.elasticat_to_model.transformer.class: CMS\HotelsBundle\Transformer\ORM\ElasticaToModelTransformer

services:
    cms.hotels.service:
        class: '%hotels.service.class%'
        arguments: ['@service_container']

    cms.hotels.rooms.service:
        class: '%hotels.rooms.service.class%'
        arguments: ['@service_container']

    cms.hotels.services.service:
        class: '%hotels.services.service.class%'
        arguments: ['@service_container']

    cms.hotels.housing.types.service:
        class: '%hotels.housing.types.service.class%'
        arguments: ['@service_container']

    cms.hotels.seasons.service:
        class: '%hotels.seasons.service.class%'
        arguments: ['@service_container']

    cms.hotels.prices.service:
        class: '%hotels.prices.service.class%'
        arguments: ['@service_container']

    cms.hotels.reservations.service:
        class: '%hotels.reservations.service.class%'
        arguments: ['@service_container']

    cms.hotels.search.provider.service:
        class: '%hotels.search.provider.class%'
        arguments:
            - '@fos_elastica.finder.springscure.hotels'
            - '@cms.hotels.prices.service'
        tags:
            - { name: cms.search.provider }

    cms.hotels.comments.service:
        class: '%hotels.comments.class%'
        arguments: ['@service_container']

    # form types services #
    cms.hotels.form.type.seasons:
        class: '%hotels.form.type.seasons.class%'
        tags:
            - { name: form.type, alias: cms_hotels_seasons_type }

    cms.hotels.form.type.season:
        class: '%hotels.form.type.season.class%'
        tags:
            - { name: form.type, alias: cms_hotels_season_type }

    cms.hotels.form.type.reservations_guests:
        class: '%hotels.form.type.reservations_guests.class%'
        tags:
            - { name: form.type, alias: cms_hotels_reservations_guests_type }

    cms.hotels.form.type.reservations_adult:
        class: '%hotels.form.type.reservations_guest.class%'
        tags:
            - { name: form.type, alias: cms_hotels_reservations_guest_type }

    # twig #
    cms.hotels.twig:
        class: '%hotels.twig.class%'
        arguments: ['@service_container']
        tags:
            - { name: twig.extension }

    cms.hotels.reservations.twig:
        class: '%hotels.reservations.twig.class%'
        arguments: ['@service_container']
        tags:
            - { name: twig.extension }

    # listeners #
    cms.hotels.listener:
        class: '%hotels.listener.class%'
        arguments: ['@cms.hotels.elastica.provider']
        tags:
            - {name: doctrine.event_listener, event: postPersist, method: postPersist}
            - {name: doctrine.event_listener, event: postUpdate, method: postUpdate}

    cms.hotels.rooms.listener:
        class: '%hotels.rooms.listener.class%'
        arguments: ['@cms.hotels.rooms.service']
        tags:
            - {name: doctrine.event_listener, event: preUpdate, method: preUpdate }
            - {name: doctrine.event_listener, event: postPersist, method: postPersist}
            - {name: doctrine.event_listener, event: postUpdate, method: postUpdate }

    cms.hotels.wizard.listener:
        class: '%hotels.wizard.listener.class%'
        arguments: ['@cms.geo.maps.service']
        tags:
          - { name: kernel.event_listener, event: form.wizard.flush, method: onWizardFlush }
          - { name: kernel.event_listener, event: form.wizard.build_form, method: onBuildForm }

    cms.hotels.reservations.listener:
        class: '%hotels.reservations.listener.class%'
        arguments: ['@cms.hotels.reservations.service']
        tags:
            - { name: doctrine.event_listener, event: prePersist, method: prePersist }
            - { name: doctrine.event_listener, event: preUpdate, method: preUpdate }

    # validators #
    cms.hotels.reservation.validator:
        class: '%hotels.reservation.validator.class%'
        arguments: ['@cms.hotels.prices.service', '@cms.hotels.reservations.service', '@cms.hotels.service']
        tags:
            - { name: validator.constraint_validator, alias: ReservationValidator }

    cms.hotels.reservation.date_start.validator:
        class: '%hotels.reservation.date_start.validator.class%'
        arguments:
            - '@cms.hotels.reservations.service'
            - '@cms.hotels.prices.service'
        tags:
            - { name: validator.constraint_validator, alias: reservation_date_start.validator }

    cms.hotels.reservation.date_end.validator:
        class: '%hotels.reservation.date_end.validator.class%'
        arguments:
            - '@cms.hotels.reservations.service'
            - '@cms.hotels.prices.service'
        tags:
            - { name: validator.constraint_validator, alias: reservation_date_end.validator }

    cms.hotels.reservation.adults.validator:
        class: '%hotels.reservation.adults.validator.class%'
        arguments:
            - '@cms.hotels.rooms.service'
        tags:
            - { name: validator.constraint_validator, alias: reservation_adults.validator }

    cms.hotels.validator:
        class: '%hotels.validator.class%'
        arguments:
            - '@cms.hotels.service'
        tags:
            - { name: validator.constraint_validator, alias: hotel.validator }

    cms.hotels.room.validator:
        class: '%hotels.room.validator.class%'
        arguments:
            - '@cms.hotels.rooms.service'
        tags:
            - { name: validator.constraint_validator, alias: room.validator }

    # geometry provider #
    cms.hotels.geo.privider:
        class: '%hotels.geo.provider.class%'
        arguments: ['@cms.hotels.service', '@templating']
        tags:
            - { name: geo.info.provider }

    # elastica provider #
    cms.hotels.elastica.provider:
        class: '%hotels.elastica.hotels.provider.class%'
        arguments:
             - '@fos_elastica.index.springscure.hotels'
             - '@cms.hotels.service'
             - '@fos_elastica.finder.springscure.hotels'
             - '@logger'
        tags:
          - { name: fos_elastica.provider, index: springscure, type: hotels }

    #elastica transformers #
    cms.hotels.elastica_to_model.transformer:
        class: '%hotels.elasticat_to_model.transformer.class%'
        arguments: ['@fos_elastica.config_manager', '@doctrine']

    # security voters #
    cms.hotels.security.voter:
        class: '%hotels.security.voter.class%'
        public: false
        tags:
            - { name: security.voter }


    cms.hotels.wizard.manager:
        class: 'CMS\HotelsBundle\Wizard\Manager'
        arguments: [ 'hotel', '@session' ]