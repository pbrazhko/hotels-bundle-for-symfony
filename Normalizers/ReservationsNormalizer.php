<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 13.06.15
 * Time: 12:38
 */

namespace CMS\HotelsBundle\Normalizers;


use CMS\HotelsBundle\Entity\HotelsReservations;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;

class ReservationsNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * Denormalizes data back into an object of the given class.
     *
     * @param mixed $data data to restore
     * @param string $class the expected class to instantiate
     * @param string $format format the given data was extracted from
     * @param array $context options available to the denormalizer
     *
     * @return object
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        // TODO: Implement denormalize() method.
    }

    /**
     * Checks whether the given class is supported for denormalization by this normalizer.
     *
     * @param mixed $data Data to denormalize from.
     * @param string $type The class to which the data should be denormalized.
     * @param string $format The format being deserialized from.
     *
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        // TODO: Implement supportsDenormalization() method.
    }

    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param object $object object to normalize
     * @param string $format format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|string|bool|int|float|null
     */
    public function normalize($object, $format = null, array $context = array())
    {
        /** @var HotelsReservations $object */
        $dates = array();
        for($i = $object->getDateStart()->getTimestamp(); $i < $object->getDateEnd()->getTimestamp(); $i = $i + 86400){
            $data = new \DateTime();
            $data->setTimestamp($i);

            array_push($dates,$data->format('d.m.Y'));
        }

        return array(
            'id' => $object->getId(),
            'hotel' => $object->getHotel(),
            'room' => $object->getRoom(),
            'dates' => $dates
        );
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed $data Data to normalize.
     * @param string $format The format being (de-)serialized from or into.
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof HotelsReservations;
    }

}