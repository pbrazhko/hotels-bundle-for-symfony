<?php
namespace CMS\HotelsBundle\Normalizers;

use CMS\HotelsBundle\Entity\Hotels;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 11.06.15
 * Time: 17:30
 */
class HotelsNormalizer extends SerializerAwareNormalizer implements NormalizerInterface
{
    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param object $object object to normalize
     * @param string $format format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|string|bool|int|float|null
     */
    public function normalize($object, $format = null, array $context = array())
    {
        /** @var Hotels $object */
        return array(
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'descriptions' => $object->getDescriptions(),
            'housing_type' => $this->serializer->normalize($object->getHousingType(), $format, $context),
            'city' => $this->serializer->normalize($object->getCity(), $format, $context),
            'rooms' => $this->serializer->normalize($object->getRooms(), $format, $context),
            'rating' => $object->getRating(),
            'photos' => $this->serializer->normalize($object->getPhotos(), $format, $context)
        );
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed $data Data to normalize.
     * @param string $format The format being (de-)serialized from or into.
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Hotels;
    }

}