<?php
namespace CMS\HotelsBundle\Normalizers;

use CMS\HotelsBundle\Entity\HotelsRooms;
use Symfony\Component\Serializer\Normalizer\DenormalizableInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 11.06.15
 * Time: 17:30
 */
class HotelsRoomsNormalizer extends SerializerAwareNormalizer implements NormalizerInterface, DenormalizableInterface
{
    /**
     * Denormalizes the object back from an array of scalars|arrays.
     *
     * It is important to understand that the denormalize() call should denormalize
     * recursively all child objects of the implementor.
     *
     * @param DenormalizerInterface $denormalizer The denormalizer is given so that you
     *                                            can use it to denormalize objects contained within this object
     * @param array|scalar $data The data from which to re-create the object.
     * @param string|null $format The format is optionally given to be able to denormalize differently
     *                                            based on different input formats
     * @param array $context options for denormalizing
     */
    public function denormalize(DenormalizerInterface $denormalizer, $data, $format = null, array $context = array())
    {
        // TODO: Implement denormalize() method.
    }

    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param object $object object to normalize
     * @param string $format format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|string|bool|int|float|null
     */
    public function normalize($object, $format = null, array $context = array())
    {
        /** @var HotelsRooms $object */
        return array(
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'descriptions' => $object->getDescriptions(),
            'price_day' => $object->getPriceDay(),
            'count' => $object->getCount(),
            'price_additional_guest' => $object->getPriceAdditionalGuest(),
            'reservation_min_day' => $object->getReservationMinDays(),
            'count_rooms' => $object->getCountRooms(),
            'count_bathrooms' => $object->getCountBathrooms(),
            'count_bedrooms' => $object->getCountBedrooms(),
            'count_guests' => $object->getCountGuests(),
            'count_max_guests' => $object->getCountMaxGuests(),
            'services' => $this->serializer->normalize($object->getServices(), $format, $context)
        );
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed $data Data to normalize.
     * @param string $format The format being (de-)serialized from or into.
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof HotelsRooms;
    }

}