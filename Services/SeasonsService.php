<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 13.03.15
 * Time: 11:43
 */

namespace CMS\HotelsBundle\Services;

use CMS\CoreBundle\AbstractCoreService;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class SeasonsService extends AbstractCoreService
{

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form;
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'HotelsBundle:Seasons';
    }
}