<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 07.06.15
 * Time: 21:38
 */

namespace CMS\HotelsBundle\Services;


use CMS\CoreBundle\AbstractCoreService;
use CMS\HotelsBundle\Entity\Hotels;
use CMS\HotelsBundle\Entity\HotelsPrices;
use CMS\HotelsBundle\Entity\HotelsReservations;
use CMS\HotelsBundle\Entity\HotelsRooms;
use CMS\HotelsBundle\Exceptions\InvalidArgumentException;
use CMS\HotelsBundle\Form\ReservationsType;
use CMS\HotelsBundle\PriceCriteria;
use CMS\HotelsBundle\ReservationCriteria;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class ReservationsService extends AbstractCoreService
{
    /**
     * @var HotelsPricesService
     */
    private $priceService;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->priceService = $container->get('cms.hotels.prices.service');
    }

    /**
     * @param HotelsReservations $data
     * @return bool
     */
    public function create($data)
    {
        $dateStart = $data->getDateStart();
        $dateEnd = $data->getDateEnd();

        for ($i = $dateStart->getTimestamp(); $i <= $dateEnd->getTimestamp(); $i = $i + 86400) {
            $data->addDate(new \DateTime('@' . $i));
        }

        parent::create($data);

        return true;
    }

    public function delete($data)
    {
        $data->setStatus(HotelsReservations::RESERVATION_CANCELED);
        $data->setIsDeleted(true);

        parent::update($data);

        return true;
    }

    public function getReservations(ReservationCriteria $reservationRequests)
    {
        $nowDate = new \DateTime();
        if(null === $reservationRequests->getDateStart()){
            $reservationRequests->setDateStart('01.01.' . $nowDate->format('Y'));
        }

        if(null === $reservationRequests->getDateEnd()){
            $dateEnd = new \DateTime('01.01.' . $nowDate->format('Y'));
            $dateEnd->add(new \DateInterval('P' . ($nowDate->format('L')?366:365) . 'D'));
            $reservationRequests->setDateEnd($dateEnd);
        }

        return $this->getRepository()->getReservations($reservationRequests);
    }

    public function getReservationsByDate($hotel, array $rooms, \DateTime $date)
    {
        if ($hotel instanceof Hotels) {
            $hotel = $hotel->getId();
        }

        if (count($rooms)) {
            foreach ($rooms as $key => $room) {
                if ($room instanceof HotelsRooms) {
                    $rooms[$key] = $room->getId();
                }
            }
        }

        return $this->getRepository()->getReservationsByDate($hotel, $rooms, $date);
    }

    public function getAllowedDates($hotel, $room = null, \DateTime $dateStart, \DateTime $dateEnd, $prices)
    {
        if (!is_array($prices) && $prices instanceof Collection) {
            throw new InvalidArgumentException('Type variable prices is not supported!');
        }

        if($room instanceof HotelsRooms){
            $room = $room->getId();
        }

        $reservationsCriteria = new ReservationCriteria();
        $reservationsCriteria
            ->setHotel($hotel)
            ->addRoom($room)
            ->setDateStart($dateStart)
            ->setDateEnd($dateEnd);

        $reservations = $this->getReservations($reservationsCriteria);

        $allowedDates = [];

        /** @var HotelsPrices $price */
        foreach ($prices as $price) {
            $date = $price->getDate();

            $index = $date->format('d.m.Y');

            if(!isset($allowedDates[$index][$price->getRoom()])){
                $allowedDates[$index][$price->getRoom()] = [
                    'id' => $price->getRoom(),
                    'count' => $price->getCount()
                ];
            }

            $allowedDates[$index][$price->getRoom()]['guests'][$price->getGuests()] = $price->getPrice();
        }

        array_walk($reservations, function ($reservation) use (&$allowedDates) {
            /** @var \DateTime $dateStart */
            $dateStart = $reservation->getDateStart();
            /** @var \DateTime $dateEnd */
            $dateEnd = $reservation->getDateEnd();

            for($i = $dateStart->getTimestamp(); $i < $dateEnd->getTimestamp(); $i = $i+86400) {
                $date = new \DateTime('@' . $i);
                $index = $date->format('d.m.Y');

                if(isset($allowedDates[$index])) {
                    $allowedDates[$index][$reservation->getRoom()]['count'] = $allowedDates[$index][$reservation->getRoom()]['count'] - 1;

                    if ((int)$allowedDates[$index][$reservation->getRoom()]['count'] <= 0) {
                        unset($allowedDates[$index]);
                    }
                }
            }
        });

        return $allowedDates;
    }

    /**
     * @param HotelsReservations $hotelsReservations
     * @return string
     */
    public function calculateNights(HotelsReservations $hotelsReservations){
        $dateStart = $hotelsReservations->getDateStart();
        $dateEnd   = $hotelsReservations->getDateEnd();

        if(!$dateStart instanceof \DateTime || !$dateEnd instanceof \DateTime){
            throw new InvalidArgumentException('Dates in reservation is not valid!');
        }

        return $dateEnd->diff($dateStart)->format('%a');
    }

    /**
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @param $hotel
     * @param null $room
     * @return bool
     */
    public function checkDateIntervalForReservation(\DateTime $dateStart, \DateTime $dateEnd, $hotel, $room = null){
        $priceCriteria = new PriceCriteria();
        $priceCriteria
            ->setHotel($hotel)
            ->setDateStart($dateStart)
            ->setDateEnd($dateEnd);

        $prices = $this->priceService->getPrices($priceCriteria);

        $allowedDates = $this->getAllowedDates($hotel, $room, $dateStart, $dateEnd, $prices);

        for($i = $dateStart->getTimestamp(); $i <= $dateEnd->getTimestamp(); $i=$i+86400){
            $index = (new \DateTime('@'. $i))->format('d.m.Y');

            if(!isset($allowedDates[$index])){
                return false;
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            new ReservationsType(),
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'HotelsBundle:HotelsReservations';
    }

} 