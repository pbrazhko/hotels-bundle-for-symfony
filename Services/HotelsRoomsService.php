<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 15.06.15
 * Time: 21:22
 */

namespace CMS\HotelsBundle\Services;


use CMS\CoreBundle\AbstractCoreService;
use CMS\HotelsBundle\Entity\Hotels;
use CMS\HotelsBundle\Entity\HotelsRooms;
use CMS\HotelsBundle\Entity\HotelsRoomsRepository;
use CMS\HotelsBundle\Form\HotelsRoomsType;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class HotelsRoomsService extends AbstractCoreService
{
    const HOTELS_ROOMS_STATUS_NOT_COMPLETED = 0;
    const HOTELS_ROOMS_STATUS_COMPLETED = 1;

    public function getRoomsByHotel($id, $selectHotel = false, $selectServices = false, $selectPhotos = false)
    {
        $repository = $this->getRepository();

        /** @var HotelsRoomsRepository $repository*/
        $builder = $repository->createQueryBuilder('hr')
            ->innerJoin('CMS\HotelsBundle\Entity\Hotels', 'h', Join::LEFT_JOIN, 'h.id = hr.hotel')
            ->where('h.id = :hotel')
            ->andWhere('h.is_deleted = 0')
            ->andWhere('h.is_published = 1')
            ->setParameter('hotel', $id);

        if ($selectHotel) {
            $builder->addSelect('h');
        }

        if ($selectServices) {
            $builder->addSelect('hrs')
                ->leftJoin('hr.services', 'hrs');
        }

        if ($selectPhotos) {
            $builder->addSelect('hrp')
                ->leftJoin('hr.photos', 'hrp');
        }

        $query = $builder->getQuery();

        return $query->getResult();
    }

    public function getRoomById($id){
        $repository = $this->getRepository();

        /** @var HotelsRoomsRepository $repository*/
        $query = $repository->createQueryBuilder('hr')
            ->innerJoin('CMS\HotelsBundle\Entity\Hotels', 'h', Join::LEFT_JOIN, 'h.id = hr.hotel')
            ->where('hr.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    public function diff(HotelsRooms $originData, HotelsRooms $updateData){
        $diffFields = array();

        $originData->getTitle() === $updateData->getTitle()?:array_push($diffFields, 'title');
        $originData->getCountRooms() === $updateData->getCountRooms()?:array_push($diffFields, 'count_rooms');
        $originData->getCountBedrooms() === $updateData->getCountBedrooms()?:array_push($diffFields, 'count_bedrooms');
        $originData->getCountBathrooms() === $updateData->getCountBathrooms()?:array_push($diffFields, 'count_bathrooms');
        $originData->getPriceDay() === $updateData->getPriceDay()?:array_push($diffFields, 'price_day');
        $originData->getPriceAdditionalGuest() === $updateData->getPriceAdditionalGuest()?:array_push($diffFields, 'price_additional_guest');
        $originData->getCountGuests() === $updateData->getCountGuests()?:array_push($diffFields, 'count_guests');
        $originData->getCountMaxGuests() === $updateData->getCountMaxGuests()?:array_push($diffFields, 'count_max_guests');
        $originData->getDescriptions() === $updateData->getDescriptions()?:array_push($diffFields, 'descriptions');
        $originData->getReservationMinDays() === $updateData->getReservationMinDays()?:array_push($diffFields, 'reservation_min_days');
        $originData->getRating() === $updateData->getRating()?:array_push($diffFields, 'rating');
        $originData->getStatus() === $updateData->getStatus()?:array_push($diffFields, 'status');
        $originData->getIsPublished() === $updateData->getIsPublished()?:array_push($diffFields, 'is_published');
        $originData->getIsDeleted() === $updateData->getIsDeleted()?:array_push($diffFields, 'is_deleted');

        return $diffFields;
    }

    /**
     * Check if is need update prices
     *
     * @param array $diff
     * @return bool
     */
    public function isNeedUpdatePrices(array $diff){
        $updatedFields = array_keys($diff);

        $updatePrice = in_array('price_day', $updatedFields);
        $updatePriceAdditionalGuest = in_array('price_additional_guest', $updatedFields);
        $updateCountGuests = in_array('count_guests', $updatedFields);
        $updateMaxCountGuests = in_array('count_max_guests', $updatedFields);
        $updateCount = in_array('count', $updatedFields);

        if($updatePrice || $updateCountGuests || $updateMaxCountGuests || $updatePriceAdditionalGuest || $updateCount){
            return true;
        }

        return false;
    }

    /**
     * @param HotelsRooms $room
     * @param Hotels $hotel
     * @param array $diff Updated fields
     * @return bool
     */
    public function updateRoomPrices(HotelsRooms $room, Hotels $hotel, array $diff)
    {
        $hotelsPricesService = $this->container->get('cms.hotels.prices.service');

        if (isset($diff['count_max_guests']) || isset($diff['count_guests'])) {
            $originPriceForGuests = array();
            $updatePriceForGuests = array();

            $originCountGuests = isset($diff['count_guests']) ? reset($diff['count_guests']) : $room->getCountGuests();
            $originCountMaxGuests = isset($diff['count_max_guests']) ? reset($diff['count_max_guests']) : $room->getCountmaxGuests();
            for ($i = $originCountGuests; $i <= $originCountMaxGuests; $i++) {
                array_push($originPriceForGuests, $i);
            }

            for ($i = $room->getCountGuests(); $i <= $room->getCountMaxGuests(); $i++) {
                array_push($updatePriceForGuests, $i);
            }

            $allGuests = array_merge($originPriceForGuests, $updatePriceForGuests);
            foreach ($allGuests as $guest) {
                if (in_array($guest, $originPriceForGuests) && in_array($guest, $updatePriceForGuests)) {
                    unset($originPriceForGuests[array_search($guest, $originPriceForGuests)]);
                    unset($updatePriceForGuests[array_search($guest, $updatePriceForGuests)]);
                }
            }

            if (count($originPriceForGuests) > 0) {
                $hotelsPricesService->removePrices($hotel, $room, $originPriceForGuests);
            }

            if (count($updatePriceForGuests) > 0) {
                foreach ($updatePriceForGuests as $guests) {
                    $hotelsPricesService->addPriceForGuests(
                        $hotel->getId(),
                        $room->getId(),
                        $room->getCount(),
                        $guests,
                        $hotelsPricesService->calculatePrice($room, $guests),
                        new \DateTime(date('d.m.Y', time())),
                        $room->getDurationReservation()
                    );
                }
            }
        }

        if (isset($diff['price_day']) || isset($diff['price_additional_guest'])) {
            $hotelsPricesService->updatePrices($hotel, $room);
        }

        return true;
    }

    public function addRoomPrice(Hotels $hotel, HotelsRooms $room){
        $hotelsPricesService = $this->container->get('cms.hotels.prices.service');

        $hotelsPricesService->addPrices(
            $hotel,
            $room,
            $room->getCount(),
            new \DateTime(date('d.m.Y', time())),
            $room->getDurationReservation()
        );

        return true;
    }

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            new HotelsRoomsType(),
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'HotelsBundle:HotelsRooms';
    }
}