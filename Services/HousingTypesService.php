<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 16.02.15
 * Time: 9:40
 */

namespace CMS\HotelsBundle\Services;


use CMS\CoreBundle\AbstractCoreService;
use CMS\HotelsBundle\Form\HousingTypesType;
use CMS\HotelsBundle\Form\ServicesType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class HousingTypesService extends AbstractCoreService
{

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            new HousingTypesType(),
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'HotelsBundle:HousingTypes';
    }
}