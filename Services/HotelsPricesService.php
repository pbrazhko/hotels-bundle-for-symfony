<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 11.06.15
 * Time: 14:09
 */

namespace CMS\HotelsBundle\Services;


use CMS\CoreBundle\AbstractCoreService;
use CMS\HotelsBundle\Entity\Hotels;
use CMS\HotelsBundle\Entity\HotelsPrices;
use CMS\HotelsBundle\Entity\HotelsRooms;
use CMS\HotelsBundle\Exceptions\InvalidArgumentException;
use CMS\HotelsBundle\Form\HotelsPricesRulesType;
use CMS\HotelsBundle\PriceCriteria;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class HotelsPricesService extends AbstractCoreService
{
    /**
     * Add hotel prices
     *
     * @param Hotels $hotel
     * @param HotelsRooms|null $room
     * @param $count
     * @param \DateTime $date
     * @param int $duration
     */
    public function addPrices(Hotels $hotel, HotelsRooms $room = null, $count, \DateTime $date, $duration = 90)
    {
        $countGuests    = $room->getCountGuests();
        $countMaxGuests = $room->getCountMaxGuests();

        for($guests = $countGuests; $guests <= $countMaxGuests; $guests++) {
            $this->addPriceForGuests(
                $hotel->getId(),
                $room->getId(),
                $count,
                $guests,
                $this->calculatePrice($room, $guests),
                $date,
                $duration
            );
        }
    }

    /**
     * Update hotel prices
     *
     * @param Hotels $hotel
     * @param HotelsRooms|null $room
     * @return bool
     */
    public function updatePrices(Hotels $hotel, HotelsRooms $room){
        $builder = $this->getRepository()->createQueryBuilder('p');

        $builder
            ->andWhere('p.hotel = :hotel')
            ->andWhere('p.is_custom = 0')
            ->andWhere('p.room = :room')
            ->setParameter('hotel', $hotel->getId())
            ->setParameter('room', $room->getId());

        if (null !== ($prices = $builder->getQuery()->getResult())) {
            $em = $this->container->get('doctrine.orm.default_entity_manager');

            /** @var HotelsPrices $price */
            foreach($prices as $price){
                $price->setPrice($this->calculatePrice($room, $price->getGuests()));

                $em->persist($price);
            }

            $em->flush();
        }

        return true;
    }


    /**
     * @param Hotels $hotel
     * @param HotelsRooms|null $room
     * @param null $guests
     * @param null $dateStart
     * @param null $dateEnd
     */
    public function updateCount(Hotels $hotel, HotelsRooms $room = null, $guests = null, $dateStart = null, $dateEnd = null){
        $priceCriteria = new PriceCriteria();

        $priceCriteria
            ->setHotel($hotel)
            ->setGuests($guests)
            ->setDateStart($dateStart)
            ->setDateEnd($dateEnd);

        if($room){
            $priceCriteria->addRoom($room);
        }

        $targetObject = $room ? $room : $hotel;

        if (null !== $prices = $this->getRepository()->getPrices($priceCriteria)) {
            $em = $this->container->get('doctrine.orm.default_entity_manager');
            /** @var HotelsPrices $price */
            foreach($prices as $price){
                $price->setCount($targetObject->getCount());

                $em->persist($price);
            }

            $em->flush();
        }
    }

    /**
     * Remove prices
     *
     * @param Hotels $hotel
     * @param HotelsRooms|null $room
     * @param array $guests
     * @return bool
     */
    public function removePrices(Hotels $hotel, HotelsRooms $room = null, array $guests = array()){
        $builder = $this->getRepository()->createQueryBuilder('p');

        $builder
            ->andWhere('p.hotel = :hotel')
            ->setParameter('hotel', $hotel->getId());

        if($room){
            $builder->andWhere('p.room = :room')
                ->setParameter('room', $room->getId());
        }

        if(count($guests) > 0){
            $builder->andWhere($builder->expr()->in('p.guests', $guests));
        }

        if (null !== ($prices = $builder->getQuery()->getResult())) {
            $em = $this->container->get('doctrine.orm.default_entity_manager');

            foreach($prices as $price){
                $em->remove($price);
            }

            $em->flush();
        }

        return true;
    }

    /**
     * Add hotel prices for guests
     *
     * @param integer $hotelId
     * @param integer|null $roomId
     * @param $count
     * @param integer $guests
     * @param float $price
     * @param \DateTime $date
     * @param integer $duration
     * @return bool
     */
    public function addPriceForGuests($hotelId, $roomId = null, $count, $guests, $price, \DateTime $date, $duration)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $_d = clone $date;
        for ($i = 0; $i < $duration; $i++) {
            $hotelPrice = new HotelsPrices();

            $hotelPrice->setHotel($hotelId);
            $hotelPrice->setCount($count);
            $hotelPrice->setGuests($guests);
            $hotelPrice->setRoom($roomId);
            $hotelPrice->setDate(clone $_d);
            $hotelPrice->setPrice($price);

            $_d->add(new \DateInterval('P1D'));

            $em->persist($hotelPrice);
        }

        $em->flush();

        return true;
    }

    /**
     * Return price for guests
     *
     * @param HotelsRooms $hotelRoom
     * @param integer $guests
     * @return float|mixed
     * @throws InvalidArgumentException
     */
    public function calculatePrice(HotelsRooms $hotelRoom, $guests){
        if(!$hotelRoom instanceof Hotels && !$hotelRoom instanceof HotelsRooms){
            throw new InvalidArgumentException(sprintf('Variable \'$data\' is not supported!'));
        }

        $countGuests = $hotelRoom->getCountGuests();

        $priceOfDay = $hotelRoom->getPriceDay();
        $priceAdditionalGuestOfDay = $hotelRoom->getPriceAdditionalGuest();

        if($guests == $countGuests){
            return $priceOfDay;
        }

        return $priceOfDay+($priceAdditionalGuestOfDay * ($guests - $countGuests));
    }

    /**
     * @param array $prices
     * @param $hotel
     * @param null $room
     * @param $guests
     * @return null
     * @throws InvalidArgumentException
     */
    public function findPriceForGuests(array $prices, $hotel, $room = null, $guests){
        $price = null;

        if(count($prices) < 1) {
            return $price;
        }

        if(!is_array(reset($prices))){
            throw new InvalidArgumentException('Type elements of prices is not supported');
        }

        foreach($prices as $p){
            if($p['hotel'] == $hotel && $p['room'] == $room && $p['guests'] == $guests){
                $price = $p;
                break;
            }
        }

        return $price;
    }

    /**
     * @param PriceCriteria $priceCriteria
     *
     * @return array
     */
    public function getPrices(PriceCriteria $priceCriteria)
    {
        return $this->getRepository()->getPrices($priceCriteria);
    }

    /**
     * @param int $hotel
     * @param array $rooms
     * @param null $guests
     * @param array $dates
     * @return array
     */
    public function getPricesForDates($hotel, array $rooms = array(), $guests = null, array $dates = array()){
        array_walk($dates, function (&$v, $k){
            /** @var $v \DateTime */
            if (!$v instanceof \DateTime){
                $v = new \DateTime($v);
            }

            $v = $v->format('Y-m-d');
        });

        if($hotel instanceof Hotels){
            $hotel = $hotel->getId();
        }

        if(count($rooms)){
            foreach($rooms as $key=>$room){
                if($room instanceof HotelsRooms){
                    $rooms[$key] = $room->getId();
                }
            }
        }

        return $this->getRepository()->getPricesForDates($hotel, $rooms, $guests, $dates);
    }

    /**
     * @param $dateStart
     * @param $dateEnd
     * @param $priceStart
     * @param $priceEnd
     * @return mixed
     */
    public function getPriceByDateAndPriceInterval($dateStart, $dateEnd, $priceStart, $priceEnd)
    {
        return $this->getRepository()->getPriceByDateAndPriceInterval($dateStart, $dateEnd, $priceStart, $priceEnd);
    }

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            new HotelsPricesType(),
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'HotelsBundle:HotelsPrices';
    }

}