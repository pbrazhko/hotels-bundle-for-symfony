<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 15.02.15
 * Time: 18:10
 */

namespace CMS\HotelsBundle\Services;


use CMS\CoreBundle\AbstractCoreService;
use CMS\HotelsBundle\Entity\Hotels;
use CMS\HotelsBundle\Form\HotelsAndRoomType;
use CMS\HotelsBundle\Form\HotelsFullType;
use CMS\HotelsBundle\Form\HotelsType;
use CMS\HotelsBundle\HotelCriteria;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class HotelsService extends AbstractCoreService
{
    const HOTELS_STATUS_NOT_COMPLETED = 0;
    const HOTELS_STATUS_COMPLETED = 1;

    public function getHotel(HotelCriteria $hotelCriteria)
    {
        return $this->getRepository()->getHotel($hotelCriteria);
    }

    public function getHotelByOwner($owner, $selectRooms = false, $selectServices = false, $selectPhotos = false)
    {
        $builder = $this->getRepository()->createQueryBuilder('h');

        $builder
            ->select(array('h', 'ht'))
            ->innerJoin('h.housing_type', 'ht')
            ->andWhere('h.create_by = :id')
            ->andWhere('h.is_deleted = 0')
            ->setParameter('id', $owner);

        if ($selectRooms) {
            $builder->addSelect('hr')
                ->leftJoin('h.rooms', 'hr', Join::WITH, 'hr.is_published = 1 AND hr.is_deleted = 0');
        }

        if ($selectServices) {
            $builder->addSelect('hs')
                ->leftJoin('h.services', 'hs');
        }

        if ($selectPhotos) {
            $builder->addSelect('hp')
                ->leftJoin('h.photos', 'hp');
        }

        $query = $builder->getQuery();

        return $query->getResult();
    }

    public function getHotelByIdAndOwner($id, $owner, $selectRooms = false, $selectServices = false, $selectPhotos = false)
    {
        $builder = $this->getRepository()->createQueryBuilder('h');

        $builder
            ->select(array('h', 'ht'))
            ->innerJoin('h.housing_type', 'ht')
            ->andWhere('h.id = :id')
            ->andWhere('h.create_by = :owner')
            ->andWhere('h.is_deleted = 0')
            ->andWhere('h.is_published = 1')
            ->setParameters(array(
                'id' => $id,
                'owner' => $owner
            ));

        if ($selectRooms) {
            $builder->addSelect('hr')
                ->leftJoin('h.rooms', 'hr', Join::WITH, 'hr.is_published = 1 AND hr.is_deleted = 0');
        }

        if ($selectServices) {
            $builder->addSelect('hs')
                ->leftJoin('h.services', 'hs');
        }

        if ($selectPhotos) {
            $builder->addSelect('hp')
                ->leftJoin('h.photos', 'hp');
        }

        $query = $builder->getQuery();

        return $query->getOneOrNullResult();
    }

    public function getHotelById($id, $selectRooms = false, $selectPhotos = false)
    {
        $builder = $this->getRepository()->createQueryBuilder('h');

        $builder
            ->select(array('h', 'ht', 'hl', 'hlg'))
            ->innerJoin('h.housing_type', 'ht')
            ->leftJoin('h.location', 'hl')
            ->innerJoin('hl.geometry', 'hlg')
            ->andWhere('h.id = :id')
            ->andWhere('h.is_deleted = 0')
            ->andWhere('h.is_published = 1')
            ->setParameter('id', $id);

        if($selectRooms){
            $builder->addSelect('hr')
                ->leftJoin('h.rooms', 'hr', Join::WITH, 'hr.is_published = 1 AND hr.is_deleted = 0');
        }

        if($selectPhotos){
            $builder->addSelect('hp')
                ->leftJoin('h.photos', 'hp');
        }

        $query = $builder->getQuery();

        return $query->getOneOrNullResult();
    }

    public function getHotelByIdAndRoom($id, $room, $selectServices = false, $selectPhotos = false)
    {
        $builder = $this->getRepository()->createQueryBuilder('h');

        $builder
            ->select(array('h', 'ht', 'hr'))
            ->innerJoin('h.housing_type', 'ht')
            ->innerJoin('h.rooms', 'hr', Join::WITH, ' hr.id = :room AND hr.is_published = 1 AND hr.is_deleted = 0')
            ->andWhere('h.id = :id')
            ->andWhere('h.is_deleted = 0')
            ->andWhere('h.is_published = 1')
            ->setParameters(array('id' => $id, 'room' => $room));

        if ($selectServices) {
            $builder->addSelect('hs')
                ->leftJoin('h.services', 'hs');
        }

        if ($selectPhotos) {
            $builder->addSelect('hp')
                ->leftJoin('h.photos', 'hp');
        }

        $query = $builder->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * Check if is need update prices
     *
     * @param array $diff
     * @return bool
     */
    public function isNeedUpdatePrices(array $diff){
        $updatedFields = array_keys($diff);

        $updatePrice = in_array('price_day', $updatedFields);
        $updatePriceAdditionalGuest = in_array('price_additional_guest', $updatedFields);
        $updateCountGuests = in_array('count_guests', $updatedFields);
        $updateMaxCountGuests = in_array('count_max_guests', $updatedFields);
        $updateCount = in_array('count', $updatedFields);

        if($updatePrice || $updateCountGuests || $updateMaxCountGuests || $updatePriceAdditionalGuest || $updateCount){
            return true;
        }

        return false;
    }

    public function addHotelPrices(Hotels $hotel){
        $hotelsPricesService = $this->container->get('cms.hotels.prices.service');

        $hotelsPricesService->addPrices(
            $hotel,
            null,
            $hotel->getCount(),
            new \DateTime(date('d.m.Y', time())),
            $hotel->getDurationReservation()
        );

        return true;
    }

    /**
     * @param Hotels $hotel
     * @param array $diff Updated fields
     * @return bool
     */
    public function updateHotelPrices(Hotels $hotel, array $diff)
    {
        $hotelsPricesService = $this->container->get('cms.hotels.prices.service');

        if (isset($diff['count_max_guests']) || isset($diff['count_guests'])) {
            $originPriceForGuests = array();
            $updatePriceForGuests = array();

            $originCountGuests = isset($diff['count_guests']) ? reset($diff['count_guests']) : $hotel->getCountGuests();
            $originCountMaxGuests = isset($diff['count_max_guests']) ? reset($diff['count_max_guests']) : $hotel->getCountmaxGuests();
            for ($i = $originCountGuests; $i <= $originCountMaxGuests; $i++) {
                array_push($originPriceForGuests, $i);
            }

            for ($i = $hotel->getCountGuests(); $i <= $hotel->getCountMaxGuests(); $i++) {
                array_push($updatePriceForGuests, $i);
            }

            $allGuests = array_merge($originPriceForGuests, $updatePriceForGuests);
            foreach ($allGuests as $guest) {
                if (in_array($guest, $originPriceForGuests) && in_array($guest, $updatePriceForGuests)) {
                    unset($originPriceForGuests[array_search($guest, $originPriceForGuests)]);
                    unset($updatePriceForGuests[array_search($guest, $updatePriceForGuests)]);
                }
            }

            if (count($originPriceForGuests) > 0) {
                $hotelsPricesService->removePrices($hotel, null, $originPriceForGuests);
            }

            if (count($updatePriceForGuests) > 0) {
                foreach ($updatePriceForGuests as $guests) {
                    $hotelsPricesService->addPriceForGuests(
                        $hotel->getId(),
                        null,
                        $hotel->getCount(),
                        $guests,
                        $hotelsPricesService->calculatePrice($hotel, $guests),
                        new \DateTime(date('d.m.Y', time())),
                        $hotel->getDurationReservation()
                    );
                }
            }
        }

        if (isset($diff['price_day']) || isset($diff['price_additional_guest'])) {
            $hotelsPricesService->updatePrices($hotel);
        }

        if(isset($diff['count'])){
            $hotelsPricesService->updateCount($hotel);
        }

        return true;
    }

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'HotelsBundle:Hotels';
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @param array $options
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null, array $options = [])
    {
        $options = array_merge($options, ['data_class' => Hotels::class]);

        return $form->createBuilder(
            HotelsType::class,
            $data, $options
        );
    }
}