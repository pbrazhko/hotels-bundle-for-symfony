<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 31.07.15
 * Time: 10:42
 */

namespace CMS\HotelsBundle\Services;


use CMS\CoreBundle\AbstractCoreService;
use CMS\HotelsBundle\CommentCriteria;
use CMS\HotelsBundle\Entity\HotelsComments;
use CMS\HotelsBundle\Form\HotelsCommentsType;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class HotelsCommentsService extends AbstractCoreService
{
    public function getCommentsByHotel($hotel, $selectUsers = false){
        $builder = $this->getRepository()->createQueryBuilder('c');

        $builder
            ->select('c')
            ->andWhere('c.hotel = :hotel')
            ->setParameter('hotel', $hotel);

        if($selectUsers){
            $builder
                ->addSelect('cu')
                ->leftJoin('c.user', 'cu');
        }

        return $builder->getQuery()->getResult();
    }

    public function getPublishedCommentsByHotel($hotel, $selectUsers = false){
        $builder = $this->getRepository()->createQueryBuilder('c');

        $builder
            ->select('c')
            ->andWhere('c.hotel = :hotel')
            ->andWhere('c.status = :status')
            ->setParameters(array(
                'hotel' => $hotel,
                'status' => HotelsComments::COMMENT_STATUS_PUBLISHED
            ));

        if($selectUsers){
            $builder
                ->addSelect('cu')
                ->leftJoin('c.user', 'cu');
        }

        return $builder->getQuery()->getResult();
    }

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            HotelsCommentsType::class,
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'HotelsBundle:HotelsComments';
    }

    public function getComments(CommentCriteria $commentRequest)
    {
        return $this->getRepository()->getComments($commentRequest);
    }
}