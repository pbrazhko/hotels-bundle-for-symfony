<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 03.09.15
 * Time: 16:30
 */

namespace CMS\HotelsBundle;


interface Validator
{
    public function validate($data);
}