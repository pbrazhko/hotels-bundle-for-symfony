<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 04.09.15
 * Time: 22:10
 */

namespace CMS\HotelsBundle;


use CMS\HotelsBundle\Entity\Hotels;
use CMS\HotelsBundle\Entity\HotelsRooms;

class ReservationCriteria
{
    private $hotel;

    private $rooms = array();

    private $dateStart;

    private $dateEnd;

    private $status;

    private $is_deleted = false;


    /**
     * @return mixed
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * @param mixed $hotel
     *
     * @return ReservationCriteria
     */
    public function setHotel($hotel)
    {
        if($hotel instanceof Hotels){
            $hotel = $hotel->getId();
        }

        $this->hotel = $hotel;

        return $this;
    }

    /**
     * @return array
     */
    public function getRooms()
    {
        return $this->rooms;
    }


    /**
     * @param $room
     *
     * @return $this
     */
    public function addRoom($room)
    {
        if($room instanceof HotelsRooms){
            $room = $room->getId();
        }

        if (!in_array($room, $this->rooms) && null !== $room) {
            array_push($this->rooms, $room);
        }

        return $this;
    }

    /**
     * @param array $rooms
     *
     * @return ReservationCriteria
     */
    public function setRooms($rooms)
    {
        foreach($rooms as $room){
            if($room instanceof HotelsRooms){
                $room = $room->getId();
            }

            if(!in_array($room, $this->rooms)){
                array_push($this->rooms, $room);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * @param mixed $dateStart
     *
     * @return ReservationCriteria
     */
    public function setDateStart($dateStart)
    {
        if(is_string($dateStart)){
            $dateStart = new \DateTime($dateStart);
        }

        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @param mixed $dateEnd
     *
     * @return ReservationCriteria
     */
    public function setDateEnd($dateEnd)
    {
        if(is_string($dateEnd)){
            $dateEnd = new \DateTime($dateEnd);
        }

        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param boolean $is_deleted
     * @return $this
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;

        return $this;
    }

}