<?php
namespace CMS\HotelsBundle\Command;

use CMS\HotelsBundle\Entity\Hotels;
use CMS\HotelsBundle\Entity\HotelsRooms;
use CMS\HotelsBundle\Exceptions\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 11.06.15
 * Time: 14:14
 */
class HotelsPricesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('cms:hotels-prices-build')
            ->setDescription('Build price for hotel of 6 month ago!');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $hotelsService = $container->get('cms.hotels.service');
        $hotelsPricesService = $container->get('cms.hotels.prices.service');

        $hotelsBuilder = $hotelsService->getRepository()->createQueryBuilder('h');

        $query = $hotelsBuilder
            ->select(array('h', 'hr'))
            ->leftJoin('h.rooms', 'hr')
            ->andWhere('h.is_deleted = 0')
            ->andWhere('h.is_published = 1')
            ->getQuery();

        if (null === ($hotels = $query->getResult())) {
            $output->writeln(sprintf('Hotels not found'));
            return;
        }

        $output->writeln(sprintf('Find %s hotels', count($hotels)));

        $hotelsPricesRepository = $hotelsPricesService->getRepository();

        $nowDate = new \DateTime(date('d.m.Y', time()));

        /** @var Hotels $hotel */
        foreach ($hotels as $hotel) {
            $output->writeln(sprintf('Hotel: %s', $hotel->getTitle()['en']));

            /** @var HotelsRooms $room */
            foreach($hotel->getRooms() as $room){
                $duration = $room->getDurationReservation();

                $output->writeln(sprintf('Room: %s', $room->getTitle()['en']));

                $allPrices = $hotelsPricesRepository->getLastPriceForRoom($hotel->getId(), $room->getid());

                for($i = $room->getCountGuests(); $i <= $room->getCountMaxGuests(); $i++){
                    $startDate = clone $nowDate;

                    $output->writeln('Guest: ' . $i);

                    try{
                        $price = $hotelsPricesService->findPriceForGuests($allPrices, $hotel->getId(), $room->getId(), $i);
                    } catch (InvalidArgumentException $e) {
                        $price = null;
                    }

                    if ($price) {
                        $lastDate = new \DateTime($price['max_date']);
                        $existsPriceOfDay = $lastDate->diff($nowDate)->format('%a')+1;

                        if($lastDate->getTimestamp() > $startDate->getTimestamp()) {
                            $startDate->add(new \DateInterval('P' . $existsPriceOfDay . 'D'));
                            $duration = $duration - $existsPriceOfDay;
                        }
                    }

                    $hotelsPricesService->addPriceForGuests(
                        $hotel->getId(),
                        $room->getId(),
                        $room->getCount(),
                        $i,
                        $hotelsPricesService->calculatePrice($room, $i),
                        $startDate,
                        $duration
                    );
                }
            }
        }
    }
}