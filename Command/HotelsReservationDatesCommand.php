<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 14.10.15
 * Time: 16:54
 */

namespace CMS\HotelsBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HotelsReservationDatesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('cms:hotels-reservations-dates-build')
            ->setDescription('After remove this command');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $reservationService = $container->get('cms.hotels.reservations.service');
        $em = $container->get('doctrine.orm.entity_manager');

        $reservationRepository = $reservationService->getRepository();
        $reservationBuilder = $reservationRepository->createQueryBuilder('r');

        $reservationBuilder
            ->select('r')
            ->leftJoin('r.dates', 'rd');

        $reservations = $reservationBuilder->getQuery()->getResult();

        if ($reservations) {
            foreach ($reservations as $reservation) {
                if (!count($reservation->getDates())) {
                    $dateStart = $reservation->getDateStart();
                    $dateEnd = $reservation->getDateEnd();

                    for ($i = $dateStart->getTimestamp(); $i <= $dateEnd->getTimestamp(); $i = $i + 86400) {
                        $reservation->addDate(new \DateTime('@' . $i));
                    }

                    $em->persist($reservation);
                }
            }

            $em->flush();
        }
    }
}