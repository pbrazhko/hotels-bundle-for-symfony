<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 07.09.15
 * Time: 12:54
 */

namespace CMS\HotelsBundle;


interface CriteriaInterface
{
    public function getHash();
}