<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 11.06.15
 * Time: 17:35
 */

namespace CMS\HotelsBundle\Response;

use CMS\HotelsBundle\Normalizers\DateTimeNormalizer;
use CMS\HotelsBundle\Normalizers\HotelsNormalizer;
use CMS\HotelsBundle\Normalizers\HotelsPricesNormalizer;
use CMS\HotelsBundle\Normalizers\HotelsRoomsNormalizer;
use CMS\HotelsBundle\Normalizers\ReservationsNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

class JsonResponse extends Response
{

    public function __construct($data, $status = 200, $headers = array())
    {
        $serializer = $this->getSerializer();

        $headers['Content-Type'] = 'application/json';

        parent::__construct($serializer->serialize($data, 'json'), $status, $headers);
    }

    private function getSerializer()
    {
        return new Serializer(
            [
                new DateTimeNormalizer(),
                new HotelsNormalizer(),
                new HotelsRoomsNormalizer(),
                new HotelsPricesNormalizer(),
                new ReservationsNormalizer()
            ],
            [new JsonEncoder()]
        );
    }
}