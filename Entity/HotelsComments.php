<?php

namespace CMS\HotelsBundle\Entity;

/**
 * HotelsComments
 */
class HotelsComments
{
    const COMMENT_STATUS_NEW = 0;
    const COMMENT_STATUS_PUBLISHED = 1;
    const COMMENT_STATUS_DELETED = 2;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var Hotels,
     */
    private $hotel;

    /**
     * @var integer
     */
    private $user;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var integer
     */
    private $status;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hotel
     *
     * @param \integer $hotel
     *
     * @return HotelsComments
     */
    public function setHotel($hotel)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return \integer
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
}

