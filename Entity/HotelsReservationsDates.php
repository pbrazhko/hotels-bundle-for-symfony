<?php

namespace CMS\HotelsBundle\Entity;

/**
 * HotelsReservationsDates
 */
class HotelsReservationsDates
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var HotelsReservations
     */
    private $reservation;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return HotelsReservationsDates
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return HotelsReservations
     */
    public function getReservation()
    {
        return $this->reservation;
    }

    /**
     * @param HotelsReservations $reservation
     * @return $this
     */
    public function setReservation($reservation)
    {
        $this->reservation = $reservation;

        return $this;
    }
}

