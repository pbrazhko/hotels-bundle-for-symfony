<?php

namespace CMS\HotelsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * HotelsReservations
 */
class HotelsReservations
{
    const
        RESERVATION_NEW = 0,
        RESERVATION_SUCCESS = 1,
        RESERVATION_CANCELED = 2;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date_start;

    /**
     * @var \DateTime
     */
    private $date_end;

    /**
     * @var int
     */
    private $nights;

    /**
     * @var ArrayCollection
     */
    private $dates;

    /**
     * @var integer
     */
    private $adults;

    /**
     * @var integer
     */
    private $children;

    /**
     * @var Hotels
     */
    private $hotel;

    /**
     * @var HotelsRooms
     */
    private $room;

    /**
     * @var float
     */
    private $price;

    /**
     * @var string
     */
    private $customer_name;

    /**
     * @var string
     */
    private $customer_last_name;

    /**
     * @var string
     */
    private $customer_phone;

    /**
     * @var string
     */
    private $customer_email;

    /**
     * @var int
     */
    private $status = self::RESERVATION_NEW;

    /**
     * @var string
     */
    private $comments;

    /**
     * @var bool
     */
    private $is_deleted = false;

    /**
     * @var ArrayCollection
     */
    private $guests;

    /**
     * @var \DateTime
     */
    private $date_create;

    /**
     * @var \DateTime
     */
    private $date_update;

    /**
     * @var integer
     */
    private $create_by;

    /**
     * @var integer
     */
    private $update_by;

    public function __construct()
    {
        $this->guests = new ArrayCollection();
        $this->dates = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return Reservation
     */
    public function setDateStart($dateStart)
    {
        $this->date_start = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->date_start;
    }

    /**
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->date_end;
    }

    /**
     * @param \DateTime $date_end
     * @return HotelsReservations
     */
    public function setDateEnd($date_end)
    {
        $this->date_end = $date_end;

        return $this;
    }

    /**
     * @return int
     */
    public function getNights()
    {
        return $this->nights;
    }

    /**
     * @param int $nights
     *
     * @return HotelsReservations
     */
    public function setNights($nights)
    {
        $this->nights = $nights;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getDates()
    {
        return $this->dates;
    }

    /**
     * @param ArrayCollection $dates
     * @return $this
     */
    public function setDates($dates)
    {
        $this->dates = $dates;

        return $this;
    }

    /**
     * @param $date
     * @return $this
     */
    public function addDate(\DateTime $date)
    {
        $hotelsReservationsDates = new HotelsReservationsDates();
        $hotelsReservationsDates
            ->setDate($date)
            ->setReservation($this);

        $this->dates->add($hotelsReservationsDates);

        return $this;
    }

    /**
     * @return int
     */
    public function getAdults()
    {
        return $this->adults;
    }

    /**
     * @param int $adults
     * @return HotelsReservations
     */
    public function setAdults($adults)
    {
        $this->adults = $adults;

        return $this;
    }

    /**
     * @return int
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param int $children
     *
     * @return HotelsReservations
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return Hotels
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * @param Hotels $hotel
     * @return HotelsReservations
     */
    public function setHotel($hotel)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * @return HotelsRooms
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param HotelsRooms $room
     * @return HotelsReservations
     */
    public function setRoom($room)
    {
        if($room instanceof HotelsRooms){
            $room = $room->getId();
        }

        $this->room = $room;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customer_name;
    }

    /**
     * @param string $customer_name
     * @return $this
     */
    public function setCustomerName($customer_name)
    {
        $this->customer_name = $customer_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerLastName()
    {
        return $this->customer_last_name;
    }

    /**
     * @param string $customer_last_name
     * @return $this
     */
    public function setCustomerLastName($customer_last_name)
    {
        $this->customer_last_name = $customer_last_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerPhone()
    {
        return $this->customer_phone;
    }

    /**
     * @param string $customer_phone
     * @return $this
     */
    public function setCustomerPhone($customer_phone)
    {
        $this->customer_phone = $customer_phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->customer_email;
    }

    /**
     * @param string $customer_email
     * @return $this
     */
    public function setCustomerEmail($customer_email)
    {
        $this->customer_email = $customer_email;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Reservation
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     * @return HotelsReservations
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param boolean $is_deleted
     * @return HotelsReservations
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getGuests()
    {
        return $this->guests;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $guests
     * @return $this
     */
    public function setGuests($guests)
    {
        $this->guests = $guests;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreateBy()
    {
        return $this->create_by;
    }

    /**
     * @param int $create_by
     * @return Reservation
     */
    public function setCreateBy($create_by)
    {
        $this->create_by = $create_by;

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateBy()
    {
        return $this->update_by;
    }

    /**
     * @param int $update_by
     * @return Reservation
     */
    public function setUpdateBy($update_by)
    {
        $this->update_by = $update_by;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * @param \DateTime $date_create
     * @return Reservation
     */
    public function setDateCreate($date_create)
    {
        $this->date_create = $date_create;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * @param \DateTime $date_update
     * @return Reservation
     */
    public function setDateUpdate($date_update)
    {
        $this->date_update = $date_update;

        return $this;
    }
}

