<?php

namespace CMS\HotelsBundle\Entity;

/**
 * HotelsRooms
 */
class HotelsRooms
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var array
     */
    private $title;

    /**
     * @var integer
     */
    private $hotel;

    /**
     * @var int
     */
    private $count_rooms = 0;

    /**
     * @var int
     */
    private $count_bedrooms = 0;

    /**
     * @var int
     */
    private $count_bathrooms = 0;

    /**
     * @var float
     */
    private $price_day = 0.0;

    /**
     * @var float
     */
    private $price_additional_guest = 0.0;

    /**
     * @var int
     */
    private $count_guests = 1;

    /**
     * @var int
     */
    private $count_max_guests = 0;

    /**
     * @var string
     */
    private $descriptions;

    /**
     * @var integer
     */
    private $duration_reservation;

    /**
     * @var int
     */
    private $reservation_min_days;

    /**
     * @var ArrayCollection
     */
    private $services;

    /**
     * @var ArrayCollection
     */
    private $photos;

    /**
     * @var float
     */
    private $rating = 1.0;

    /**
     * @var int
     */
    private $count = 1;

    /**
     * @var int
     */
    private $status = 0;

    /**
     * @var ArrayCollection
     */
    private $reservations;

    /**
     * @var bool
     */
    private $is_published = false;

    /**
     * @var bool
     */
    private $is_deleted = false;

    /**
     * @var \DateTime
     */
    private $date_create;

    /**
     * @var \DateTime
     */
    private $date_update;

    /**
     * @var integer
     */
    private $create_by;

    /**
     * @var integer
     */
    private $update_by;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param array $title
     * @return HotelsRooms
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return int
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * @param int $hotel
     * @return HotelsRooms
     */
    public function setHotel($hotel)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountRooms()
    {
        return $this->count_rooms;
    }

    /**
     * @param int $count_rooms
     * @return HotelsRooms
     */
    public function setCountRooms($count_rooms)
    {
        $this->count_rooms = $count_rooms;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountBedrooms()
    {
        return $this->count_bedrooms;
    }

    /**
     * @param int $count_bedrooms
     * @return HotelsRooms
     */
    public function setCountBedrooms($count_bedrooms)
    {
        $this->count_bedrooms = $count_bedrooms;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountBathrooms()
    {
        return $this->count_bathrooms;
    }

    /**
     * @param int $count_bathrooms
     * @return HotelsRooms
     */
    public function setCountBathrooms($count_bathrooms)
    {
        $this->count_bathrooms = $count_bathrooms;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceDay()
    {
        return $this->price_day;
    }

    /**
     * @param float $price_day
     * @return HotelsRooms
     */
    public function setPriceDay($price_day)
    {
        $this->price_day = $price_day;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceAdditionalGuest()
    {
        return $this->price_additional_guest;
    }

    /**
     * @param float $price_additional_guest
     * @return HotelsRooms
     */
    public function setPriceAdditionalGuest($price_additional_guest)
    {
        $this->price_additional_guest = $price_additional_guest;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountGuests()
    {
        return $this->count_guests;
    }

    /**
     * @param int $count_guests
     * @return HotelsRooms
     */
    public function setCountGuests($count_guests)
    {
        $this->count_guests = $count_guests;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountMaxGuests()
    {
        return $this->count_max_guests;
    }

    /**
     * @param int $count_max_guests
     * @return HotelsRooms
     */
    public function setCountMaxGuests($count_max_guests)
    {
        $this->count_max_guests = $count_max_guests;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * @param string $descriptions
     * @return HotelsRooms
     */
    public function setDescriptions($descriptions)
    {
        $this->descriptions = $descriptions;

        return $this;
    }

    /**
     * @return int
     */
    public function getDurationReservation()
    {
        return $this->duration_reservation;
    }

    /**
     * @param int $duration_reservation
     * @return $this
     */
    public function setDurationReservation($duration_reservation)
    {
        $this->duration_reservation = $duration_reservation;

        return $this;
    }

    /**
     * @return int
     */
    public function getReservationMinDays()
    {
        return $this->reservation_min_days;
    }

    /**
     * @param int $reservation_min_days
     * @return HotelsRooms
     */
    public function setReservationMinDays($reservation_min_days)
    {
        $this->reservation_min_days = $reservation_min_days;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @param ArrayCollection $services
     * @return HotelsRooms
     */
    public function setServices($services)
    {
        $this->services = $services;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @param ArrayCollection $photos
     * @return HotelsRooms
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;

        return $this;
    }

    /**
     * @return float
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param float $rating
     * @return HotelsRooms
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return HotelsRooms
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return HotelsRooms
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getReservations()
    {
        return $this->reservations;
    }

    /**
     * @param ArrayCollection $reservations
     * @return HotelsRooms
     */
    public function setReservations($reservations)
    {
        $this->reservations = $reservations;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->is_published;
    }

    /**
     * @param boolean $is_published
     * @return HotelsRooms
     */
    public function setIsPublished($is_published)
    {
        $this->is_published = $is_published;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param boolean $is_deleted
     * @return HotelsRooms
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * @param \DateTime $date_create
     * @return HotelsRooms
     */
    public function setDateCreate($date_create)
    {
        $this->date_create = $date_create;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * @param \DateTime $date_update
     * @return HotelsRooms
     */
    public function setDateUpdate($date_update)
    {
        $this->date_update = $date_update;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreateBy()
    {
        return $this->create_by;
    }

    /**
     * @param int $create_by
     * @return HotelsRooms
     */
    public function setCreateBy($create_by)
    {
        $this->create_by = $create_by;

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateBy()
    {
        return $this->update_by;
    }

    /**
     * @param int $update_by
     * @return HotelsRooms
     */
    public function setUpdateBy($update_by)
    {
        $this->update_by = $update_by;

        return $this;
    }
}

