<?php

namespace CMS\HotelsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * HousingTypes
 */
class HousingTypes
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var integer
     */
    private $is_have_rooms;

    /**
     * @var ArrayCollection
     */
    private $hotels;

    /**
     * @var \DateTime
     */
    private $date_create;

    /**
     * @var \DateTime
     */
    private $date_update;

    /**
     * @var integer
     */
    private $create_by;

    /**
     * @var integer
     */
    private $update_by;

    public function __construct()
    {
        $this->hotels = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return HousingTypes
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getIsHaveRooms()
    {
        return $this->is_have_rooms;
    }

    /**
     * @param int $is_have_rooms
     * @return HousingTypes
     */
    public function setIsHaveRooms($is_have_rooms)
    {
        $this->is_have_rooms = $is_have_rooms;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getHotels()
    {
        return $this->hotels;
    }

    /**
     * @param ArrayCollection $hotels
     * @return $this
     */
    public function setHotels($hotels)
    {
        $this->hotels = $hotels;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreateBy()
    {
        return $this->create_by;
    }

    /**
     * @param int $create_by
     * @return Locale
     */
    public function setCreateBy($create_by)
    {
        $this->create_by = $create_by;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * @param \DateTime $date_create
     * @return Locale
     */
    public function setDateCreate($date_create)
    {
        $this->date_create = new \DateTime();

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * @param \DateTime $date_update
     * @return Locale
     */
    public function setDateUpdate($date_update)
    {
        $this->date_update = new \DateTime();

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateBy()
    {
        return $this->update_by;
    }

    /**
     * @param int $update_by
     * @return Locale
     */
    public function setUpdateBy($update_by)
    {
        $this->update_by = $update_by;

        return $this;
    }
}
