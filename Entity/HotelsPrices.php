<?php

namespace CMS\HotelsBundle\Entity;

/**
 * HotelsPrices
 */
class HotelsPrices
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $hotel;

    /**
     * @var integer
     */
    private $room;

    /**
     * @var integer
     */
    private $count;

    /**
     * @var integer
     */
    private $guests;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var float
     */
    private $price = 0.0;

    /**
     * @var integer
     */
    private $is_active = 1;

    /**
     * @var integer
     */
    private $is_custom = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hotel
     *
     * @param integer $hotel
     *
     * @return HotelsPrices
     */
    public function setHotel($hotel)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return integer
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * @return int
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param int $room
     * @return HotelsPricesRules
     */
    public function setRoom($room)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return $this
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return int
     */
    public function getGuests()
    {
        return $this->guests;
    }

    /**
     * @param int $guests
     * @return HotelsPrices
     */
    public function setGuests($guests)
    {
        $this->guests = $guests;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return HotelsPricesRules
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return int
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * @param int $is_active
     * @return HotelsPrices
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;

        return $this;
    }

    /**
     * @return int
     */
    public function getIsCustom()
    {
        return $this->is_custom;
    }

    /**
     * @param int $is_custom
     * @return $this
     */
    public function setIsCustom($is_custom)
    {
        $this->is_custom = $is_custom;

        return $this;
    }
}

