<?php

namespace CMS\HotelsBundle\Entity;

use CMS\GalleryBundle\Entity\Images;
use CMS\GeoBundle\Entity\Geoobjects;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Hotels
 */
class Hotels
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var locale_string
     */
    protected $title;

    /**
     * @var HousingTypes
     */
    protected $housing_type;

    /**
     * @var Cities
     */
    protected $city;

    /**
     * @var Geoobjects
     */
    protected $location;

    /**
     * @var string
     */
    protected $descriptions;

    /**
     * @var ArrayCollection
     */
    protected $photos;

    /**
     * @var float
     */
    protected $rating = 1.0;

    /**
     * @var int
     */
    protected $status = 0;

    /**
     * @var ArrayCollection
     */
    protected $rooms;

    /**
     * @var ArrayCollection
     */
    protected $comments;

    /**
     * @var bool
     */
    protected $is_published = false;

    /**
     * @var bool
     */
    protected $is_deleted = false;

    /**
     * @var \DateTime
     */
    protected $date_create;

    /**
     * @var \DateTime
     */
    protected $date_update;

    /**
     * @var integer
     */
    protected $create_by;

    /**
     * @var integer
     */
    protected $update_by;

    public function __construct()
    {
        $this->rooms = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->photos = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set housing_type
     *
     * @param $housingType
     * @return $this
     */
    public function setHousingType($housingType)
    {
        $this->housing_type = $housingType;

        return $this;
    }

    /**
     * Get housing_type
     *
     * @return \int
     */
    public function getHousingType()
    {
        return $this->housing_type;
    }

    /**
     * @return locale_string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param locale_string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return int
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param int $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Geoobjects
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Geoobjects $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * @param mixed $descriptions
     * @return $this
     */
    public function setDescriptions($descriptions)
    {
        $this->descriptions = $descriptions;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @param ArrayCollection $photos
     * @return $this
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;

        return $this;
    }

    public function addPhoto(Images $photo)
    {
        $this->photos->add($photo);

        $photo->addHotel($this);
    }

    /**
     * @return float
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param float $rating
     * @return $this
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @param ArrayCollection $rooms
     * @return $this
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;

        return $this;
    }

    public function addRoom($room)
    {
        $this->rooms[] = $room;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param ArrayCollection $comments
     * @return $this
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    public function addComment($comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->is_published;
    }

    /**
     * @param boolean $is_published
     * @return $this
     */
    public function setIsPublished($is_published)
    {
        $this->is_published = $is_published;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param boolean $is_deleted
     * @return $this
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreateBy()
    {
        return $this->create_by;
    }

    /**
     * @param int $create_by
     * @return $this
     */
    public function setCreateBy($create_by)
    {
        $this->create_by = $create_by;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * @param \DateTime $date_create
     * @return $this
     */
    public function setDateCreate($date_create)
    {
        $this->date_create = new \DateTime();

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * @param \DateTime $date_update
     * @return $this
     */
    public function setDateUpdate($date_update)
    {
        $this->date_update = new \DateTime();

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateBy()
    {
        return $this->update_by;
    }

    /**
     * @param int $update_by
     * @return $this
     */
    public function setUpdateBy($update_by)
    {
        $this->update_by = $update_by;

        return $this;
    }
}
