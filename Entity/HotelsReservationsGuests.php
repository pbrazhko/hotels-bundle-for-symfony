<?php

namespace CMS\HotelsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * ReservationsGuests
 */
class HotelsReservationsGuests
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $first_name;

    /**
     * @var string
     */
    private $last_name;

    /**
     * @var DateTime
     */
    private $birthday;

    /**
     * @var string
     */
    private $doc_number;

    /**
     * @var DateTime
     */
    private $doc_date_issue;

    /**
     * @var string
     */
    private $doc_issued;

    /**
     * @var ArrayCollection
     */
    private $reservations;

    /**
     * @var \DateTime
     */
    private $date_update;

    /**
     * @var integer
     */
    private $update_by;

    public function __construct()
    {
        $this->reservations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return ReservationGuests
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     * @return $this
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param DateTime $birthday
     * @return $this
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocNumber()
    {
        return $this->doc_number;
    }

    /**
     * @param string $doc_number
     * @return $this
     */
    public function setDocNumber($doc_number)
    {
        $this->doc_number = $doc_number;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDocDateIssue()
    {
        return $this->doc_date_issue;
    }

    /**
     * @param DateTime $doc_date_issue
     * @return $this
     */
    public function setDocDateIssue($doc_date_issue)
    {
        $this->doc_date_issue = $doc_date_issue;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocIssued()
    {
        return $this->doc_issued;
    }

    /**
     * @param string $doc_issued
     * @return $this
     */
    public function setDocIssued($doc_issued)
    {
        $this->doc_issued = $doc_issued;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getReservations()
    {
        return $this->reservations;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $reservations
     * @return $this
     */
    public function setReservations($reservations)
    {
        $this->reservations = $reservations;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * @param \DateTime $date_update
     * @return $this
     */
    public function setDateUpdate($date_update)
    {
        $this->date_update = $date_update;

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdateBy()
    {
        return $this->update_by;
    }

    /**
     * @param int $update_by
     * @return $this
     */
    public function setUpdateBy($update_by)
    {
        $this->update_by = $update_by;

        return $this;
    }
}

