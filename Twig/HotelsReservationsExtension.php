<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 15.08.15
 * Time: 21:30
 */

namespace CMS\HotelsBundle\Twig;


use CMS\HotelsBundle\Entity\HotelsReservations;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig_Environment;

class HotelsReservationsExtension extends \Twig_Extension
{
    private $container;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
    }

    /**
     * Returns a list of filters to add to the existing list.
     *
     * @return array An array of filters
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('count_reservation', array($this, 'countReservation'))
        );
    }

    public function countReservation(array $reservations, $hotel = null, $room = null, $date){
        if (count($reservations) < 1 && reset($reservations) instanceof HotelsReservations) {
            return false;
        }

        if(is_string($date)){
            $date = new \DateTime($date);
        }

        $result = array();
        /** @var HotelsReservations $reservation */
        foreach($reservations as $reservation){
            if($hotel && $reservation->getHotel() != $hotel){
                continue;
            }

            if($room && $reservation->getRoom() != $room){
                continue;
            }

            if($date->getTimestamp() < $reservation->getDateStart()->getTimestamp() || $date->getTimestamp() > $reservation->getDateEnd()->getTimestamp()){
                continue;
            }

            $result[] = $reservation;
        }

        return $result;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'cms_hotels_reservation_extension';
    }

}