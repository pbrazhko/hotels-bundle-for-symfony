<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 23.02.15
 * Time: 19:40
 */

namespace CMS\HotelsBundle\Twig;

use CMS\HotelsBundle\Entity\Hotels;
use CMS\HotelsBundle\Entity\HotelsPricesRules;
use CMS\HotelsBundle\Exceptions\InvalidArgumentException;
use CMS\HotelsBundle\Form\HotelsPricesRulesType;
use CMS\HotelsBundle\PriceCriteria;
use CMS\HotelsBundle\ReservationCriteria;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig_Environment;

class HotelsExtension extends \Twig_Extension
{

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('show_users_hotels', array($this, 'showUsersHotels'), array('is_safe' => array('html'), 'needs_environment' => true)),
            new \Twig_SimpleFunction('show_users_hotels_rooms', array($this, 'showUsersHotelsRooms'), array('is_safe' => array('html'), 'needs_environment' => true)),
            new \Twig_SimpleFunction('show_users_hotels_reservations', array($this, 'showUsersHotelsReservations'), array('is_safe' => array('html'), 'needs_environment' => true)),
            new \Twig_SimpleFunction('show_hotel_rooms', array($this, 'showHotelRooms'), array('is_safe' => array('html'), 'needs_environment' => true)),
            new \Twig_SimpleFunction('show_hotel_calendar', array($this, 'showHotelCalendar'), array('is_safe' => array('html'), 'needs_environment' => true)),
            new \Twig_SimpleFunction('show_hotel_comments', array($this, 'showHotelComments'), array('is_safe' => array('html'), 'needs_environment' => true)),
            new \Twig_SimpleFunction('show_hotel_comments_form', array($this, 'showHotelCommentsForm'), array('is_safe' => array('html'), 'needs_environment' => true))
        );
    }

    public function showUsersHotels(\Twig_Environment $environment, $userId, $template = 'HotelsBundle:Twig:hotels.html.twig')
    {
        $service = $this->container->get('cms.hotels.service');

        $hotels = array();
        $result = $service->getHotelByOwner($userId, true);

        if ($result) {
            $hotels = $result;
        }

        return $environment->render($template, array(
            'hotels' => $hotels
        ));
    }

    public function showUsersHotelsRooms(Twig_Environment $environment, $userId, $hotel, $template = 'HotelsBundle:Twig:hotels.html.twig'){
        $service = $this->container->get('cms.hotels.rooms.service');

        $rooms = array();
        $result = $service->findBy(array(
            'is_deleted' => false,
            'create_by' => $userId,
            'hotel' => $hotel
        ));

        if ($result) {
            $rooms = $result;
        }

        return $environment->render($template, array(
            'rooms' => $rooms
        ));
    }

    public function showUsersHotelsReservations(Twig_Environment $environment, $userId, $template = 'HotelsBundle:Twig:reservations.html.twig'){
        $hotelsService = $this->container->get('cms.hotels.service');

        /** @var HotelsRepository $hotelsRepository */
        $hotelsRepository = $hotelsService->getRepository();

        $hotelsReservationsService = $this->container->get('cms.hotels.reservations.service');

        /** @var ReservationsRepository $hotelsReservationsRepository */
        $hotelsReservationsRepository = $hotelsReservationsService->getRepository();

        $hotelsCriteria = Criteria::create();
        $reservationsCriteria = Criteria::create();

        $hotelsCriteria
            ->andWhere($hotelsCriteria->expr()->eq('create_by', $userId))
            ->andWhere($hotelsCriteria->expr()->eq('is_deleted', 0))
            ->andWhere($hotelsCriteria->expr()->eq('is_published', 1));

        $hotelsResult = $hotelsRepository->matching($hotelsCriteria);

        $reservations = array();
        if($hotelsResult) {
            $hotels = array();

            foreach($hotelsResult as $hotel){
                $hotels[$hotel->getId()] = $hotel;

                if(count($hotel->getRooms())){
                    $rooms = array();

                    foreach($hotel->getRooms()as $room){
                        $rooms[$room->getId()] = $room;
                    }

                    $hotel->setRooms($rooms);
                }
            }

            $reservationsCriteria
                ->andWhere($reservationsCriteria->expr()->in('hotel', array_keys($hotels)));

            $reservations = $hotelsReservationsRepository->matching($reservationsCriteria);
        }

        return $environment->render($template, array(
            'hotels' => $hotels,
            'reservations' => $reservations
        ));
    }

    public function showHotelRooms(Twig_Environment $environment, $hotel, $prices = array(), $template = 'HotelsBundle:Twig:rooms.html.twig'){
        $hotelsService = $this->container->get('cms.hotels.service');
        $hotelsRoomsService = $this->container->get('cms.hotels.rooms.service');

        if(!$hotel instanceof Hotels && !is_numeric($hotel)){
            throw new InvalidArgumentException('Hotel is not supported type!');
        }

        if(is_numeric($hotel) && null === ($hotel = $hotelsService->getHotelById($hotel, true, true, true))){
            return;
        }

        $rooms = $hotelsRoomsService->getRoomsByHotel($hotel->getId(), false, true, true);

        return $environment->render($template, array(
            'hotel' => $hotel->getId(),
            'rooms' => $rooms,
            'prices' => $prices
        ));
    }

    public function showHotelCalendar(Twig_Environment $environment, $year, Hotels $hotel, array $rooms = array(), $template = 'HotelsBundle:Twig:calendar.html.twig')
    {
        $nowDate = new \DateTime('01.01.' . $year);

        $pricesService = $this->container->get('cms.hotels.prices.service');
        $reservationService = $this->container->get('cms.hotels.reservations.service');

        $priceCriteria = new PriceCriteria();
        $priceCriteria
            ->setHotel($hotel)
            ->setRooms($rooms)
            ->setDateStart($nowDate->format('Y') . '-01-01')
            ->setDateEnd($nowDate->format('Y') . '-12-31');

        $reservationRequest = new ReservationCriteria();
        $reservationRequest
            ->setHotel($hotel)
            ->setRooms($rooms)
            ->setDateStart(new \DateTime($nowDate->format('Y') . '-01-01'))
            ->setDateEnd(new \DateTime($nowDate->format('Y') . '-12-31'));

        $pricesResult = $pricesService->getPrices($priceCriteria);

        $prices = array();
        foreach ($pricesResult as $price) {
            if ($hotel->getHousingType()->getIsHaveRooms()) {
                $prices[$price->getDate()->format('d.m.Y')][$price->getRoom()] = array(
                    'count' => $price->getCount(),
                    'price' => $price->getPrice(),
                    'is_active' => $price->getIsActive()
                );
            } else {
                $prices[$price->getDate()->format('d.m.Y')] = array(
                    'count' => $price->getCount(),
                    'price' => $price->getPrice(),
                    'is_active' => $price->getIsActive()
                );
            }
        }

        $reservations = $reservationService->getReservations($reservationRequest);

        return $environment->render($template, array(
            'hotel' => $hotel,
            'rooms' => $rooms,
            'prices' => $prices,
            'reservations' => $reservations,
            'year' => $year
        ));
    }

    public function showHotelComments(Twig_Environment $environment, $hotel, $template = 'HotelsBundle:Twig:comments.html.twig')
    {
        $commentsService = $this->container->get('cms.hotels.comments.service');

        if ($hotel instanceof Hotels) {
            $hotel = $hotel->getid();
        }

        return $environment->render($template, array(
            'comments' => $commentsService->getPublishedCommentsByHotel($hotel, true)
        ));
    }

    public function showHotelCommentsForm(Twig_Environment $environment, $hotel, $template = 'HotelsBundle:Twig:comments_form.html.twig')
    {
        if(!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')){
            return false;
        }

        $commentsService = $this->container->get('cms.hotels.comments.service');

        $form = $commentsService->generateForm();

        if ($hotel instanceof Hotels) {
            $hotel = $hotel->getid();
        }

        $form->get('hotel')->setData($hotel);

        return $environment->render($template, array(
            'form' => $form->createView()
        ));
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'cms_hotels_extension';
    }
}