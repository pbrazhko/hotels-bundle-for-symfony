<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 07.09.15
 * Time: 12:53
 */

namespace CMS\HotelsBundle;


abstract class AbstractCriteria implements CriteriaInterface
{
    private $useQueryCache = false;

    private $useResultCache = false;

    private $queryCacheTTL = 86400;

    private $resultCacheTTL = 86400;

    /**
     * @return boolean
     */
    public function isUseQueryCache()
    {
        return $this->useQueryCache;
    }

    /**
     * @param boolean $useQueryCache
     * @return $this
     */
    public function setUseQueryCache($useQueryCache)
    {
        $this->useQueryCache = $useQueryCache;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isUseResultCache()
    {
        return $this->useResultCache;
    }

    /**
     * @param boolean $useResultCache
     * @return $this
     */
    public function setUseResultCache($useResultCache)
    {
        $this->useResultCache = $useResultCache;

        return $this;
    }

    /**
     * @return int
     */
    public function getQueryCacheTTL()
    {
        return $this->queryCacheTTL;
    }

    /**
     * @param int $queryCacheTTL
     * @return $this
     */
    public function setQueryCacheTTL($queryCacheTTL)
    {
        $this->queryCacheTTL = $queryCacheTTL;

        return $this;
    }

    /**
     * @return int
     */
    public function getResultCacheTTL()
    {
        return $this->resultCacheTTL;
    }

    /**
     * @param int $resultCacheTTL
     * @return $this
     */
    public function setResultCacheTTL($resultCacheTTL)
    {
        $this->resultCacheTTL = $resultCacheTTL;

        return $this;
    }

    public function getHash()
    {
        return md5(serialize($this));
    }
}