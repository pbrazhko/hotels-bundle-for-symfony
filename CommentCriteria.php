<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 07.09.15
 * Time: 12:33
 */

namespace CMS\HotelsBundle;


use CMS\HotelsBundle\Entity\Hotels;
use CMS\UsersBundle\Entity\Users;

class CommentCriteria extends AbstractCriteria
{
    private $hotel;

    private $type;

    private $status;

    private $date;

    private $user;

    /**
     * @return mixed
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * @param mixed $hotel
     * @return $this
     */
    public function setHotel($hotel)
    {
        if ($hotel instanceof Hotels) {
            $hotel = $hotel->getId();
        }

        $this->hotel = $hotel;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return $this
     */
    public function setUser($user)
    {
        if ($user instanceof Users) {
            $user = $user->getId();
        }

        $this->user = $user;

        return $this;
    }
}