<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 07.06.15
 * Time: 21:41
 */

namespace CMS\HotelsBundle\Controller;


use CMS\HotelsBundle\Entity\HotelsReservations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HotelsReservationsController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('HotelsBundle:Reservations:list.html.twig', array(
            'reservations' => $this->get('cms.hotels.reservations.service')->findAll()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $service = $this->get('cms.hotels.reservations.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                /** @var HotelsReservations $data */
                $data = $form->getData();

                $data->setCountGuests(count($data->getGuests()));

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_hotels_reservations_list'));
            }
        }

        return $this->render('HotelsBundle:Reservations:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction(Request $request, $id)
    {
        $service = $this->get('cms.hotels.reservations.service');

        $reservation = $service->findOneById($id);

        $form = $service->generateForm($reservation);

        $form->get('hotel')->setData($reservation->getHotel());

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->update($form->getData());

                return $this->redirect($this->generateUrl('cms_hotels_reservations_list'));
            }
        }

        return $this->render('HotelsBundle:Reservations:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $service = $this->get('cms.hotels.reservations.service');

        if (false === $reservation = $service->getOnById($id)) {
            throw $this->createNotFoundException('Reservation not found!');
        }

        $reservation->setIsDeleted(true);

        $service->update($reservation);

        return $this->redirect($this->generateUrl('cms_hotels_reservations_list'));
    }
} 