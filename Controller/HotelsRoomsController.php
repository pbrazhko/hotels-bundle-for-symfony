<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 17.06.15
 * Time: 20:52
 */

namespace CMS\HotelsBundle\Controller;


use CMS\HotelsBundle\Entity\HotelsRooms;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HotelsRoomsController extends Controller
{
    public function indexAction($hotel){
        return $this->render('HotelsBundle:HotelsRooms:list.html.twig', array(
            'hotel' => $hotel,
            'rooms' => $this->get('cms.hotels.rooms.service')->getRoomsByHotel($hotel)
        ));
    }

    public function createAction(Request $request, $hotel){
        $service = $this->get('cms.hotels.rooms.service');

        if(null === ($hotel = $this->get('cms.hotels.service')->findOneById($hotel))){
            throw $this->createNotFoundException('Hotel not found!');
        }

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                /** @var HotelsRooms $data */
                $data->setHotel($hotel);

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_hotels_rooms_list', array('hotel' => $hotel->getId())));
            }
        }

        return $this->render('HotelsBundle:HotelsRooms:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction(Request $request, $hotel, $id){
        $service = $this->get('cms.hotels.rooms.service');

        if(null === ($room = $service->getRoomById($id))){
            throw $this->createNotFoundException('Room not found!');
        }

        $form = $service->generateForm($room);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->update($form->getData());

                return $this->redirect($this->generateUrl('cms_hotels_rooms_list', array('hotel' => $hotel)));
            }
        }

        return $this->render('HotelsBundle:HotelsRooms:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id){

    }

    public function calendarAction(Request $request, $hotel, $room = null)
    {
        $hotelsService = $this->get('cms.hotels.service');

        if (null === ($hotel = $hotelsService->getHotelByIdAndRoom($hotel, $room))) {
            throw $this->createNotFoundException('Hotel not found!');
        }

        if (null !== ($rooms = $hotel->getRooms())) {
            $rooms = $rooms->toArray();
        }

        $dateNow = new \DateTime();
        $year = $request->query->get('year', $dateNow->format('Y'));

        return $this->render('HotelsBundle:HotelsRooms:calendar.html.twig', array(
            'hotel' => $hotel,
            'rooms' => $rooms,
            'year' => $year
        ));
    }
}