<?php

namespace CMS\HotelsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HotelsPricesController extends Controller
{
    public function viewAction(Request $request, $hotel, $room = null)
    {
        $hotelsService = $this->get('cms.hotels.service');
        $roomsService = $this->get('cms.hotels.rooms.service');

        if(null === ($hotel = $hotelsService->getHotelById($hotel))){
            throw $this->createNotFoundException('Hotel not found!');
        }

        $rooms = array();
        if(null === $room) {
            $rooms = $roomsService->findBy(array('hotel' => $hotel));
        }
        else{
            if(null === ($result = $roomsService->findOneBy(array('id' => $room)))){
                throw $this->createNotFoundException('Room not found');
            }

            $rooms[] = $result;
        }

        $dateNow = new \DateTime();
        $year = $request->query->get('year', $dateNow->format('Y'));

        return $this->render('HotelsBundle:HotelsPrices:view.html.twig', array(
            'hotel' => $hotel,
            'rooms' => $rooms,
            'year' => $year
        ));
    }
}
