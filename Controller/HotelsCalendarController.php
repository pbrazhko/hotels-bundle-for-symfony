<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 03.09.15
 * Time: 15:30
 */

namespace CMS\HotelsBundle\Controller;


use CMS\HotelsBundle\Entity\HotelsPrices;
use CMS\HotelsBundle\Entity\HotelsPricesRepository;
use CMS\HotelsBundle\Entity\HotelsRepository;
use CMS\HotelsBundle\HotelCriteria;
use CMS\HotelsBundle\PriceCriteria;
use CMS\HotelsBundle\Response\ErrorJsonResponse;
use CMS\HotelsBundle\Response\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HotelsCalendarController extends Controller
{
    public function informationOfDayAction($hotel, $date, $room = null)
    {
        $date = new \DateTime($date);
        $hotelsService = $this->get('cms.hotels.service');
        $pricesService = $this->get('cms.hotels.prices.service');
        $reservationService = $this->get('cms.hotels.reservations.service');

        $priceCriteria = new PriceCriteria();
        $hotelCriteria = new HotelCriteria();

        if ($room) {
            $priceCriteria->addRoom($room);
            $hotelCriteria->addRoom($room);
        }

        $hotelCriteria->setId($hotel);
        $hotelCriteria->setIsSelectRooms(true);

        if (null === ($hotel = $hotelsService->getHotel($hotelCriteria))) {
            throw $this->createNotFoundException('Hotel not found!');
        }

        $priceCriteria
            ->setHotel($hotel)
            ->setDateStart($date)
            ->setDateEnd($date);

        $prices = $pricesService->getPrices($priceCriteria);
        $reservations = $reservationService->getReservationsByDate($hotel, $hotelCriteria->getRooms(), $date);

        return new JsonResponse(['hotel' => $hotel, 'prices' => $prices, 'reservations' => $reservations]);
    }

    public function setStopSalesAction(Request $request)
    {
        if (!$this->isGranted('ROLE_USER')) {
            return new ErrorJsonResponse('Access denied!', 403);
        }

        $hotelsService = $this->get('cms.hotels.service');
        $hotelsPricesService = $this->get('cms.hotels.prices.service');

        $dates = $request->request->get('dates', array());
        $hotel = $request->request->get('hotel');
        $room = $request->request->get('room');

        if (null === ($hotel = $hotelsService->getHotelByIdAndOwner($hotel, $this->getUser()->getId(), true))) {
            return new ErrorJsonResponse('Hotel not found!', 404);
        }

        $rooms = array();
        if (null != $room) {
            $roomIsExists = false;
            foreach ($hotel->getRooms() as $hotelRoom) {
                if ($hotelRoom->getId() == $room) {
                    $roomIsExists = true;
                    break;
                }
            }

            if (!$roomIsExists) {
                return new ErrorJsonResponse('Hotel room not found!', 404);
            }
        }

        $prices = $hotelsPricesService->getPricesForDates($hotel->getId(), $rooms, null, $dates);

        if ($prices) {
            $em = $this->get('doctrine.orm.entity_manager');
            /** @var HotelsPrices $price */
            foreach ($prices as $price) {
                $price->setIsActive(false);
                $price->setIsCustom(true);
                $em->persist($price);
            }

            $em->flush();
        }

        return new JsonResponse(['status' => 'success']);
    }

    public function setStartSalesAction(Request $request)
    {
        if (!$this->isGranted('ROLE_USER')) {
            return new ErrorJsonResponse('Access denied!', 403);
        }

        $hotelsService = $this->get('cms.hotels.service');
        $hotelsPricesService = $this->get('cms.hotels.prices.service');

        $dates = $request->request->get('dates', array());
        $hotel = $request->request->get('hotel');
        $room = $request->request->get('room');

        if (null === ($hotel = $hotelsService->getHotelByIdAndOwner($hotel, $this->getUser()->getId(), true))) {
            return new ErrorJsonResponse('Hotel not found!', 404);
        }

        $rooms = array();
        if (null != $room) {
            $roomIsExists = false;
            foreach ($hotel->getRooms() as $hotelRoom) {
                if ($hotelRoom->getId() == $room) {
                    $roomIsExists = true;
                    break;
                }
            }

            if (!$roomIsExists) {
                return new ErrorJsonResponse('Hotel room not found!', 404);
            }
        }

        $prices = $hotelsPricesService->getPricesForDates($hotel->getId(), $rooms, null, $dates);

        if ($prices) {
            $em = $this->get('doctrine.orm.entity_manager');
            /** @var HotelsPrices $price */
            foreach ($prices as $price) {
                $price->setIsActive(true);
                $price->setIsCustom(true);
                $em->persist($price);
            }

            $em->flush();
        }

        return new JsonResponse(['status' => 'success']);
    }

    public function changePriceAction(Request $request)
    {
        if (!$this->isGranted('ROLE_USER')) {
            return new ErrorJsonResponse('Access denied!', 403);
        }

        $hotelsService = $this->get('cms.hotels.service');
        $hotelsPricesService = $this->get('cms.hotels.prices.service');

        $dates = $request->request->get('dates', array());
        $hotel = $request->request->get('hotel');
        $room = $request->request->get('room');
        $newPrice = $request->request->get('price');

        if ($newPrice <= 0) {
            return new ErrorJsonResponse('Price is invalid');
        }

        if (null === ($hotel = $hotelsService->getHotelByIdAndOwner($hotel, $this->getUser()->getId(), true))) {
            return new ErrorJsonResponse('Hotel not found!', 404);
        }

        $rooms = array();
        if (null != $room) {
            $roomIsExists = false;
            foreach ($hotel->getRooms() as $hotelRoom) {
                if ($hotelRoom->getId() == $room) {
                    $roomIsExists = true;
                    break;
                }
            }

            if (!$roomIsExists) {
                return new ErrorJsonResponse('Hotel room not found!', 404);
            }
        }

        $prices = $hotelsPricesService->getPricesForDates($hotel->getId(), $rooms, null, $dates);

        if ($prices) {
            $em = $this->get('doctrine.orm.entity_manager');
            /** @var HotelsPrices $price */
            foreach ($prices as $price) {
                $price->setPrice($newPrice);
                $price->setIsCustom(true);
                $em->persist($price);
            }

            $em->flush();
        }

        return new JsonResponse(['status' => 'success']);
    }

    public function changeCountAction(Request $request)
    {
        if (!$this->isGranted('ROLE_USER')) {
            return new ErrorJsonResponse('Access denied!', 403);
        }

        $hotelsService = $this->get('cms.hotels.service');
        $hotelsPricesService = $this->get('cms.hotels.prices.service');

        $dates = $request->request->get('dates', array());
        $hotel = $request->request->get('hotel');
        $room = $request->request->get('room');
        $count = $request->request->get('count');

        if ($count < 0) {
            return new ErrorJsonResponse('Count is invalid');
        }

        if (null === ($hotel = $hotelsService->getHotelByIdAndOwner($hotel, $this->getUser()->getId(), true))) {
            return new ErrorJsonResponse('Hotel not found!', 404);
        }

        $rooms = array();
        if (null != $room) {
            $roomIsExists = false;
            foreach ($hotel->getRooms() as $hotelRoom) {
                if ($hotelRoom->getId() == $room) {
                    $roomIsExists = true;
                    break;
                }
            }

            if (!$roomIsExists) {
                return new ErrorJsonResponse('Hotel room not found!', 404);
            }
        }

        $prices = $hotelsPricesService->getPricesForDates($hotel->getId(), $rooms, null, $dates);

        if ($prices) {
            $em = $this->get('doctrine.orm.entity_manager');
            /** @var HotelsPrices $price */
            foreach ($prices as $price) {
                $price->setCount($count);
                $price->setIsCustom(true);
                $em->persist($price);
            }

            $em->flush();
        }

        return new JsonResponse(['status' => 'success']);
    }
}