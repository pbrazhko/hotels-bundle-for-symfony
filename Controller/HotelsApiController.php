<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 11.06.15
 * Time: 13:57
 */

namespace CMS\HotelsBundle\Controller;

use CMS\HotelsBundle\Entity\HotelsPrices;
use CMS\HotelsBundle\PriceCriteria;
use CMS\HotelsBundle\ReservationCriteria;
use CMS\HotelsBundle\Response\ErrorJsonResponse;
use CMS\HotelsBundle\Response\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HotelsApiController extends Controller
{
    public function informationAction($hotel, $room = null)
    {
        $hotelsService = $this->get('cms.hotels.service');
        $pricesService = $this->get('cms.hotels.prices.service');
        $reservationService = $this->get('cms.hotels.reservations.service');

        $priceCriteria = new PriceCriteria();
        $reservationCriteria = new ReservationCriteria();

        $reservationSubject = null;

        if ($room) {
            $hotel = $hotelsService->getHotelByIdAndRoom($hotel, $room, true);

            $reservationSubject = reset($hotel->getRooms());
            $priceCriteria->addRoom($reservationSubject);
            $reservationCriteria->addRoom($reservationSubject);
        }
        else{
            $hotel = $hotelsService->getHotelById($hotel, true);
            $reservationSubject = $hotel;
        }

        if (!$hotel || ($hotel->getHousingType()->getIsHaveRooms() && null === $room)) {
            throw $this->createNotFoundException('Hotel not found!');
        }

        $currentDate = new \DateTime();
        $dateStart = new \DateTime($currentDate->format('Y/m/') . '01');
        $dateEnd = $currentDate->add(
            new \DateInterval('P' . $reservationSubject->getDurationReservation() . 'D')
        );

        $priceCriteria
            ->setHotel($hotel)
            ->setDateStart($dateStart)
            ->setDateEnd($dateEnd);

        $reservationCriteria
            ->setHotel($hotel)
            ->setDateStart($dateStart)
            ->setDateEnd($dateEnd);

        $prices = $pricesService->getPrices($priceCriteria);
        $reservation = $reservationService->getReservations($reservationCriteria);

        return new JsonResponse(['hotel' => $hotel, 'prices' => $prices, 'reservations' => $reservation]);
    }

    public function priceAction(Request $request){
        $hotel = $request->request->get('hotel');
        $room = $request->request->get('room');
        $dateStart = $request->request->get('date_start');
        $dateEnd = $request->request->get('date_end');
        $guests = $request->request->get('guests');

        if (empty($hotel) || empty($dateStart) || empty($dateEnd) || empty($guests)){
            return new ErrorJsonResponse('errors.price.required_fields');
        }

        $hotelsService = $this->get('cms.hotels.service');
        $hotelsPriceService = $this->get('cms.hotels.prices.service');

        $targetObject = null;

        $priceCriteria = new PriceCriteria();

        $rooms = array();
        if($room){
            if(null === ($hotel = $hotelsService->getHotelByIdAndRoom($hotel, $room))){
                return new ErrorJsonResponse('errors.hotel_not_found');
            }

            $hotelRooms = $hotel->getRooms()->toArray();
            $priceCriteria->addRoom(reset($hotelRooms));
            $targetObject = $room;
        }
        else{
            if(null === ($hotel = $hotelsService->getHotelById($hotel))){
                return new ErrorJsonResponse('errors.hotel_not_found');
            }

            $targetObject = $hotel;
        }

        if($targetObject->getCountMaxGuests() < $guests){
            return new ErrorJsonResponse('errors.price.max_guests');
        }

        $priceCriteria
            ->setHotel($hotel)
            ->setGuests($guests)
            ->setDateStart($dateStart)
            ->setDataEnd($dateEnd);

        $prices = $hotelsPriceService->getPrices($priceCriteria);

        $result = 0;
        if(null !== $prices){
            /** @var HotelsPrices $price */
            foreach($prices as $price){
                $result = $result + $price->getPrice();
            }
        }

        return new JsonResponse(['price' => $result]);
    }
}