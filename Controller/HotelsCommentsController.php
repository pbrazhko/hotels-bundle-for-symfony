<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 31.07.15
 * Time: 12:34
 */

namespace CMS\HotelsBundle\Controller;


use CMS\HotelsBundle\Entity\HotelsComments;
use CMS\HotelsBundle\Exceptions\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HotelsCommentsController extends Controller
{
    /**
     * @param $hotel
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($hotel)
    {
        $commentsService = $this->get('cms.hotels.comments.service');

        $comments = $commentsService->getCommentsByHotel($hotel, true);

        $statuses = $this->getCommentsStatuses();

        return $this->render('HotelsBundle:Comments:list.html.twig', array(
            'comments' => $comments,
            'statuses' => $statuses,
            'hotel' => $hotel
        ));
    }

    /**
     * @param $comment
     * @param $status
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeStatusAction($hotel, $comment, $status){
        $commentsService = $this->get('cms.hotels.comments.service');

        /** @var HotelsComments $comment */
        if(null === ($comment = $commentsService->findOneById($comment))){
            throw $this->createNotFoundException('Comment not found');
        }

        $statuses = $this->getCommentsStatuses();

        if(!in_array($status, $statuses)){
            throw new InvalidArgumentException('Status is not defined');
        }

        $comment->setStatus($status);

        $commentsService->update($comment);

        return $this->redirect($this->generateUrl('cms_hotels_comments_list', array('hotel' => $hotel)));
    }

    private function getCommentsStatuses(){
        return  array(
            'New' => HotelsComments::COMMENT_STATUS_NEW,
            'Delete' => HotelsComments::COMMENT_STATUS_DELETED,
            'Published' => HotelsComments::COMMENT_STATUS_PUBLISHED
        );
    }
}