<?php

namespace CMS\HotelsBundle\Controller;

use CMS\GalleryBundle\Entity\Images;
use CMS\HotelsBundle\Entity\Hotels;
use CMS\HotelsBundle\Form\HotelWizard\HotelMainType;
use CMS\HotelsBundle\Form\HotelWizard\HotelPhotosType;
use CMS\HotelsBundle\Form\HotelWizard\HotelPlacementType;
use CMS\HotelsBundle\Form\HotelWizard\HotelPriceType;
use CMS\HotelsBundle\Form\HotelWizard\HotelReservationType;
use CMS\HotelsBundle\Form\HotelWizard\HotelRoomsType;
use CMS\HotelsBundle\Form\HotelWizard\HotelServicesType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class HotelsController
 * @package CMS\HotelsBundle\Controller
 */
class HotelsController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $service = $this->get('cms.hotels.service');

        return $this->render('HotelsBundle:Hotels:list.html.twig', array(
            'hotels' => $service->findAll()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $service = $this->get('cms.hotels.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $service->create($data);

                if (!$data->getHousingType()->getIsHaveRooms()) {
                    return $this->redirect($this->generateUrl('cms_hotels_rooms_create', ['hotel' => $data->getId()]));
                }

                return $this->redirect($this->generateUrl('cms_hotels_list'));
            }
        }

        return $this->render('HotelsBundle:Hotels:edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction(Request $request, $id)
    {
        $service = $this->get('cms.hotels.service');

        /** @var Hotels $hotel */
        if (null === ($hotel = $service->getHotelById($id))) {
            throw $this->createNotFoundException('Hotel not found');
        }

        $form = $service->generateForm($hotel);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->update($form->getData());

                return $this->redirect($this->generateUrl('cms_hotels_list'));
            }
        }

        return $this->render('HotelsBundle:Hotels:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $hotelsService = $this->get('cms.hotels.service');

        $registry = $this->getDoctrine();

        if (null === ($hotel = $hotelsService->getHotelById($id, true))) {
            throw $this->createNotFoundException('Hotel not found!');
        }

        $hotelsRoomsEM = $registry->getManagerForClass(Hotels::class);
        $hotelsEM = $registry->getManagerForClass(Hotels::class);
        $photosEM = $registry->getManagerForClass(Images::class);

        foreach ($hotel->getRooms() as $room) {
            $hotelsRoomsEM->remove($room);
        }

        foreach ($hotel->getPhotos() as $photo) {
            $photosEM->remove($photo);
        }

        $hotelsEM->remove($hotel);

        $photosEM->flush();
        $hotelsRoomsEM->flush();
        $hotelsEM->flush();

        return $this->redirect($this->generateUrl('cms_hotels_list'));
    }

    /**
     * @param Request $request
     * @param $hotel
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function calendarAction(Request $request, $hotel)
    {
        $hotelsService = $this->get('cms.hotels.service');

        if (null === ($hotel = $hotelsService->getHotelById($hotel, true))) {
            throw $this->createNotFoundException('Hotel not found!');
        }

        if (null !== ($rooms = $hotel->getRooms())) {
            $rooms = $rooms->toArray();
        }

        $dateNow = new \DateTime();
        $year = $request->query->get('year', $dateNow->format('Y'));

        return $this->render('HotelsBundle:Hotels:calendar.html.twig', array(
            'hotel' => $hotel,
            'rooms' => $rooms,
            'year' => $year
        ));
    }
}
