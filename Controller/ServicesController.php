<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 16.02.15
 * Time: 9:39
 */

namespace CMS\HotelsBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ServicesController
 * @package CMS\HotelsBundle\Controller
 */
class ServicesController extends Controller
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $service = $this->get('cms.hotels.services.service');

        return $this->render('HotelsBundle:Services:list.html.twig', array(
            'services' => $service->findAll()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $service = $this->get('cms.hotels.services.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $data->setCreateBy($this->getUser()->getId());

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_hotels_services_list'));
            }
        }

        return $this->render('HotelsBundle:Services:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction(Request $request, $id)
    {
        $service = $this->get('cms.hotels.services.service');

        $form = $service->generateForm($service->findOneById($id));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $data->setUpdateBy($this->getUser()->getId());

                $service->update($data);

                return $this->redirect($this->generateUrl('cms_hotels_services_list'));
            }
        }

        return $this->render('HotelsBundle:Services:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $service = $this->get('cms.hotels.services.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_hotels_services_list'));
    }
}